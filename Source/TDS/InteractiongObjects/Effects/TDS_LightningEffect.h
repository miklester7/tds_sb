// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractiongObjects/Effects/TDS_MasterEffect.h"
#include "TDS_LightningEffect.generated.h"

class UParticleSystem;

UCLASS()
class TDS_API UTDS_LightningEffect : public UTDS_MasterEffect
{
	GENERATED_BODY()
	
public:
	virtual bool ExecuteEffect(AActor* EffectOwner) override;

	void AddDamage();

	FTimerHandle AddDamageTimerHandle;

	virtual void EffectDestroy() override;

	UPROPERTY(EditDefaultsOnly, Category = "FX")
	UParticleSystem* EffectFX;

	UPROPERTY(EditDefaultsOnly, Category = "Effect")
	TSubclassOf<UDamageType> DamageType;

	UPROPERTY(EditDefaultsOnly,Category = "Effect")
	float minDamage = 2.f;

	UPROPERTY(EditDefaultsOnly, Category = "Effect")
	float maxDamage = 2.f;

	UPROPERTY(EditDefaultsOnly, Category = "Effect")
	float DamageRadius = 150.f;

	UPROPERTY(EditAnywhere, Category = "Effect")
		float Frequency = 0.5f;
private:
	UParticleSystemComponent* ActiveEffect = nullptr;
};
 