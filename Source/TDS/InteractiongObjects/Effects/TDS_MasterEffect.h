// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "TDS_MasterEffect.generated.h"


UCLASS(Blueprintable)
class TDS_API UTDS_MasterEffect : public UObject
{
	GENERATED_BODY()

public:
	void InitEffect(AActor* EffectOwner);

	UPROPERTY(EditAnywhere,BlueprintReadWrite, Category = "Effect", meta = (EditCondition = "!bIsExecutedOnce"))
	float EffectLifeTime = 10.f;

	UPROPERTY(EditAnywhere, Category = "Effect")
	bool bIsExecutedOnce = false;
	
	FTimerHandle LifeTimeTimerHandle;

	virtual bool ExecuteEffect(AActor* EffectOwner);

	AActor* Owner = nullptr;

	virtual void EffectDestroy();
	
};
