// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractiongObjects/Effects/TDS_MasterEffect.h"

void UTDS_MasterEffect::InitEffect(AActor* EffectOwner)
{
	if (!bIsExecutedOnce)
	{
		GetWorld()->GetTimerManager().SetTimer(LifeTimeTimerHandle, this,&UTDS_MasterEffect::EffectDestroy, 0.01f, false, EffectLifeTime);
	}
	Owner = EffectOwner;

	ExecuteEffect(EffectOwner); //If PlaySound
}

void UTDS_MasterEffect::EffectDestroy()
{
	this->ConditionalBeginDestroy();
}

bool UTDS_MasterEffect::ExecuteEffect(AActor* EffectOwner)
{
	return false;
}
