// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractiongObjects/Effects/TDS_LightningEffect.h"
#include "Particles/ParticleSystem.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "PlayerCharacter.h"

DEFINE_LOG_CATEGORY_STATIC(LightningEffectLog, All, All);

bool UTDS_LightningEffect::ExecuteEffect(AActor* EffectOwner)
{
	if (!EffectFX) return false;

	ActiveEffect = UGameplayStatics::SpawnEmitterAttached(EffectFX, EffectOwner->GetRootComponent());

	GetWorld()->GetTimerManager().SetTimer(AddDamageTimerHandle, this, &UTDS_LightningEffect::AddDamage, Frequency, !bIsExecutedOnce);

	return true;
}

void UTDS_LightningEffect::AddDamage()
{
	if (!DamageType)
	{
		UE_LOG(LightningEffectLog, Error, TEXT("f:AddDamage, DamageType: nullptr"));
		return;
	}
	const TArray<AActor*, FDefaultAllocator> IgnorActors;

	const float Damage = FMath::FRandRange(minDamage, maxDamage);

	UGameplayStatics::ApplyRadialDamage(
		GetWorld(),
		Damage,
		Owner->GetActorLocation(),
		DamageRadius,
		DamageType,
		IgnorActors,
		Owner,
		Owner->GetInstigatorController(),
		true);
}

void UTDS_LightningEffect::EffectDestroy()
{
	ActiveEffect->DestroyComponent();

	GetWorld()->GetTimerManager().ClearTimer(AddDamageTimerHandle);

	const auto Player = Cast<APlayerCharacter>(Owner);
	if (Player)
	{
		Player->RemoveEffectFromArray(this);
	}
	else
	{
		UE_LOG(LightningEffectLog, Error, TEXT("f:EffectDestroy, Player: nullptr"));
	}
	Super::EffectDestroy();
}
