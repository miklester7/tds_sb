// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "TDSDoors.generated.h"

class USoundCue;
class UBoxComponent;
class USceneComponent;
class USoundCue;

UCLASS()
class TDS_API ATDSDoors : public AActor
{
	GENERATED_BODY()
	
public:	
	ATDSDoors();

	UPROPERTY(VisibleAnywhere,BlueprintReadWrite)
	UStaticMeshComponent* Door;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sounds")
	USoundCue* OpeningDoorSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sounds")
	USoundCue* ClosingDoorSound;

	UPROPERTY(VisibleAnywhere)
	UBoxComponent* BoxComponent;

	UPROPERTY(VisibleAnywhere)
	USceneComponent* DefaultRootComponent;

	UFUNCTION()
	void DoorInteracting(UPrimitiveComponent* OverlappedComponent, AActor*
		OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void EndOverlapBox(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	void DoorOpening();

	void DoorClosing();

	bool bIsOpened = false;
	bool bIsDoorMovement = false;

private:

	FTimerHandle DoorOpenTimerHandle;
	FTimerHandle DoorCloseTimerHandle;
	FVector CurrentLocation = FVector::ZeroVector;

	int32 ActorAround = 0;
};
