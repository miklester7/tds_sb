// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS_BaseReceivingDamagesActor.generated.h"

class UTDS_BaseHealthComponent;
class ATDS_GDamageActor;

UCLASS()
class TDS_API ATDS_BaseReceivingDamagesActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ATDS_BaseReceivingDamagesActor();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UTDS_BaseHealthComponent* HealthComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite,Category = "Widgets")
	TSubclassOf<ATDS_GDamageActor> DamageActor;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "SpawnVector", meta = (MakeEditWidget = true))
		FVector DamageWidgetLocation;

	UFUNCTION()
	void SpawnDamageWidget(const float Damage,FVector& SpawnLocation,const FLinearColor& Color);

protected:
	virtual void BeginPlay() override;


};
