// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Types.h"
#include "TDS_PickupBase.generated.h"

class UNiagaraComponent;
class UStaticMeshComponent;
class USceneComponent;
class USphereComponent;

UCLASS()
class TDS_API ATDS_PickupBase : public AActor
{
	GENERATED_BODY()
	
public:	
	ATDS_PickupBase();

	virtual bool ExecutionEffects(AActor* Player);
	virtual void OnCompletedEffect();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	bool bIsCanRespawn = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	float RespawnTimer = 1.f;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Components")
	UNiagaraComponent* NiagaraComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
	USphereComponent* SphereCollision;

	FTimerHandle RespawnTimerHandle;
protected:
	virtual void BeginPlay() override;

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	void HidePickupActor();
	void ShowPickupActor();

};
