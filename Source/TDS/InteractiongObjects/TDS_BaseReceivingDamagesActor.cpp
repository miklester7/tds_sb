// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractiongObjects/TDS_BaseReceivingDamagesActor.h"
#include "Components/TDS_BaseHealthComponent.h"
#include "UI/TDS_DamageWidget.h"
#include "UI/TDS_GDamageActor.h"

DEFINE_LOG_CATEGORY_STATIC(DamageActorLog, All, All);

ATDS_BaseReceivingDamagesActor::ATDS_BaseReceivingDamagesActor()
{
	PrimaryActorTick.bCanEverTick = false;

	HealthComponent = CreateDefaultSubobject<UTDS_BaseHealthComponent>(TEXT("HealthComponent"));
	HealthComponent->TakeDamage.AddUObject(this, &ATDS_BaseReceivingDamagesActor::SpawnDamageWidget);
}

void ATDS_BaseReceivingDamagesActor::BeginPlay()
{
	Super::BeginPlay();
	
	ensure(DamageActor);
}

void ATDS_BaseReceivingDamagesActor::SpawnDamageWidget(const float Damage, FVector& SpawnLocation, const FLinearColor& Color)
{
	if (SpawnLocation.IsZero())
	{
		SpawnLocation = GetActorLocation() + DamageWidgetLocation;
		UE_LOG(DamageActorLog, Display, TEXT("Widget location %s"), *SpawnLocation.ToString());
	}

	SpawnLocation = SpawnLocation + FMath::FRandRange(-10.f, 10.f);
	 
	ATDS_GDamageActor* Widget = GetWorld()->SpawnActor<ATDS_GDamageActor>(DamageActor, SpawnLocation, FRotator::ZeroRotator, FActorSpawnParameters());
	if (!Widget) return;

	Widget->SetWidgetInfo(Damage,Color);
}
