// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractiongObjects/TDSMainInteractionObject.h"
#include "Components/TDSInventoryComponent.h"
#include "Components/SphereComponent.h"
#include "Engine/SkeletalMesh.h"
#include "Engine/StaticMesh.h"
#include "TDSGameInstance.h"
#include "PlayerCharacter.h"
#include "TDSUtils.h"

DEFINE_LOG_CATEGORY_STATIC(PickupLog, All, All);

ATDSMainInteractionObject::ATDSMainInteractionObject()
{
	PrimaryActorTick.bCanEverTick = false;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	SetRootComponent(SceneComponent);

	StaticMeshSlot = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMeshSlot->SetupAttachment(GetRootComponent());
	StaticMeshSlot->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	StaticMeshSlot->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	StaticMeshSlot->SetCollisionResponseToChannel(ECollisionChannel::ECC_WorldStatic, ECollisionResponse::ECR_Block);
	StaticMeshSlot->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Ignore);
	StaticMeshSlot->SetGenerateOverlapEvents(false);
	StaticMeshSlot->SetSimulatePhysics(true);
	StaticMeshSlot->SetRenderCustomDepth(true);

	SphereCollison = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollison"));
	SphereCollison->SetupAttachment(StaticMeshSlot);
	SphereCollison->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	SphereCollison->SetGenerateOverlapEvents(false);

}

void ATDSMainInteractionObject::CanTake()
{
	SphereCollison->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	SphereCollison->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	SphereCollison->SetCollisionResponseToChannel(ECollisionChannel::ECC_Pawn, ECollisionResponse::ECR_Overlap);
	SphereCollison->SetGenerateOverlapEvents(true);
}

void ATDSMainInteractionObject::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	const auto PlayerPawn = Cast<APlayerCharacter>(OtherActor);
	if (!PlayerPawn) return;

	const auto Invenory = TDSUtils::GetTDSComponentByClass<UTDSInventoryComponent>(PlayerPawn);
	if (!Invenory) return;

	if (CurrentItemInfo.bIsAmmo)
	{
		if (Invenory->SetWeaponAmmoForType(CurrentItemInfo.Type))
		{
			PlayerPawn->UpdateHUDAllAmmo(CurrentItemInfo.Type);
			PlayerPawn->UpdateAllAmmo();
			Destroy();
		}
	}
	else
	{
		if (PlayerPawn->TryGetNewItem(ObjectName))
		{
			Invenory->SetAmmoInClipForType(CurrentItemInfo.Type, CurrentItemInfo.AmmoInClip);
			Destroy();
		}
	}
}

void ATDSMainInteractionObject::ItemInit(FName ItemName)
{
	const auto GameInstancee = Cast<UTDSGameInstance>(GetGameInstance());
	if (!GameInstancee) return;

	FDropItemsInfo ItemInfo;
	if (GameInstancee->GetItemsInfoByName(ItemName, ItemInfo))
	{
		CurrentItemInfo.Type = ItemInfo.Type;
		CurrentItemInfo.bIsAmmo = ItemInfo.bIsAmmo;
		ObjectName = ItemName;
		SphereCollison->SetSphereRadius(ItemInfo.CollisionRadius);

		StaticMeshSlot->SetStaticMesh(ItemInfo.ItemMesh);

		if (CurrentItemInfo.bIsAmmo)
		{
			StaticMeshSlot->SetWorldScale3D(FVector(ItemInfo.Scale));
			CanTake();
		}
		else
		{
			GetWorldTimerManager().SetTimer(CanTakeTimerHandl, this, &ATDSMainInteractionObject::CanTake, 0.01f, false, 0.5f);
		}

		SetDepthByType();
	}
}

void ATDSMainInteractionObject::SetDepthByType()
{
	switch (CurrentItemInfo.Type)
	{
	case EWeaponType::Rifle_Type:
		StaticMeshSlot->SetCustomDepthStencilValue(3);
		break;
	case EWeaponType::Sniper_Type:
		StaticMeshSlot->SetCustomDepthStencilValue(4);;
		break;
	case EWeaponType::Shotgun_Type:
		StaticMeshSlot->SetCustomDepthStencilValue(2);
		break;
	case EWeaponType::Launcher_Type:
		StaticMeshSlot->SetCustomDepthStencilValue(4);
		break;
	default:
		break;
	}
}

