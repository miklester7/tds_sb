// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractiongObjects/Pickups/TDS_PickupLightning.h"
#include "PlayerCharacter.h"
#include "InteractiongObjects/Effects/TDS_LightningEffect.h"

DEFINE_LOG_CATEGORY_STATIC(LightningEffectL, All, All);

bool ATDS_PickupLightning::ExecutionEffects(AActor* Player)
{
	APlayerCharacter* PlayerCharacter = Cast<APlayerCharacter>(Player);

	if (!LightningEffectClass)
	{
		UE_LOG(LightningEffectL, Error, TEXT("LightningEffectClass is nullpt"));
		return false;
	}

	const auto Effects = PlayerCharacter->GetAllCurrentEffects();

	for (int8 i = 0; i < Effects.Num(); i++)
	{
		if (Effects[i]->GetClass() == LightningEffectClass) return false; // mb add time and Damage?
	}

	const auto Effect = NewObject<UTDS_LightningEffect>(PlayerCharacter, LightningEffectClass);
	if (!Effect) return false;

	Effect->InitEffect(Player);

	PlayerCharacter->CurrentEffects.Add(Effect);

	return true;
}