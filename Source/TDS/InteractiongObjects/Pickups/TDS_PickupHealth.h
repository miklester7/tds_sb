// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractiongObjects/TDS_PickupBase.h"
#include "TDS_PickupHealth.generated.h"


UCLASS()
class TDS_API ATDS_PickupHealth : public ATDS_PickupBase
{
	GENERATED_BODY()
	
public:
	virtual bool ExecutionEffects(AActor* Player) override;

	UPROPERTY(EditAnywhere, Category = "Effect")
	float AddHealth = 20.f;
};
