// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractiongObjects/TDS_PickupBase.h"
#include "TDS_AddShieldPickup.generated.h"

UCLASS()
class TDS_API ATDS_AddShieldPickup : public ATDS_PickupBase
{
	GENERATED_BODY()

public:
	virtual bool ExecutionEffects(AActor* Player) override;

	UPROPERTY(EditAnywhere, Category = "Effect")
	float AddShield = 200.f;
};
