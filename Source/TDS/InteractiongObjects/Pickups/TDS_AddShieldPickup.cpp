// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractiongObjects/Pickups/TDS_AddShieldPickup.h"
#include "Components/TDS_CharacterHealthComponent.h"
#include "PlayerCharacter.h"
#include "TDSUtils.h"

DEFINE_LOG_CATEGORY_STATIC(AddShieldPickupLog, All, All);

bool ATDS_AddShieldPickup::ExecutionEffects(AActor* Player)
{
	const auto HealthComponnet = TDSUtils::GetTDSComponentByClass<UTDS_CharacterHealthComponent>(Player);
	if (!HealthComponnet) return false;

	UE_LOG(AddShieldPickupLog, Warning, TEXT("Percent: %f"), HealthComponnet->GetAdditionalShieldPercent());
	if (HealthComponnet->GetAdditionalShieldPercent() == 1) return false;

	HealthComponnet->SetAdditionalShield(AddShield);

	return true;
}