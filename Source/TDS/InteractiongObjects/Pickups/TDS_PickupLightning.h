// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "InteractiongObjects/TDS_PickupBase.h"
#include "TDS_PickupLightning.generated.h"

class UTDS_LightningEffect;

UCLASS()
class TDS_API ATDS_PickupLightning : public ATDS_PickupBase
{
	GENERATED_BODY()
	
public:
	virtual bool ExecutionEffects(AActor* Player) override;

	UPROPERTY(EditDefaultsOnly, Category = "Effect")
	TSubclassOf<UTDS_LightningEffect> LightningEffectClass;
};
