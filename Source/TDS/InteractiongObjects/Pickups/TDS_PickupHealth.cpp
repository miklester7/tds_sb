// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractiongObjects/Pickups/TDS_PickupHealth.h"
#include "TDSUtils.h"
#include "Components/TDS_BaseHealthComponent.h"

DEFINE_LOG_CATEGORY_STATIC(HealthPickupLog, All, All);

bool ATDS_PickupHealth::ExecutionEffects(AActor* Player)
{
	auto HealthComponent = TDSUtils::GetTDSComponentByClass<UTDS_BaseHealthComponent>(Player);

	return HealthComponent->AddHealth(AddHealth);
}