// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractiongObjects/TDS_PickupBase.h"
#include "PlayerCharacter.h"
#include "Components/StaticMeshComponent.h"
#include "NiagaraComponent.h"
#include "Components/SceneComponent.h"
#include "Components/SphereComponent.h"


ATDS_PickupBase::ATDS_PickupBase()
{
	PrimaryActorTick.bCanEverTick = false;
	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponnet"));
	SetRootComponent(SceneComponent);

	NiagaraComponent = CreateDefaultSubobject<UNiagaraComponent>(TEXT("NiagaraComponent"));
	NiagaraComponent->SetupAttachment(GetRootComponent());

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMesh->SetupAttachment(GetRootComponent());
	StaticMesh->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	SphereCollision = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollison"));
	SphereCollision->SetupAttachment(GetRootComponent());
}

void ATDS_PickupBase::BeginPlay()
{
	Super::BeginPlay();	

	if (!StaticMesh || !StaticMesh->GetStaticMesh())
	{
		StaticMesh->DestroyComponent();
	}

	if (!NiagaraComponent || !NiagaraComponent->GetFXSystemAsset())
	{
		NiagaraComponent->DestroyComponent();
	}
}

void ATDS_PickupBase::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

	const APlayerCharacter* Player = Cast<APlayerCharacter>(OtherActor);

	if (!Player) return;
	
	if(!ExecutionEffects(OtherActor)) return;

	OnCompletedEffect();
}

void ATDS_PickupBase::HidePickupActor()
{
	SetActorHiddenInGame(true);

	SphereCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	if (StaticMesh)
	{
		StaticMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	}
	
	GetWorld()->GetTimerManager().SetTimer(RespawnTimerHandle, this, &ATDS_PickupBase::ShowPickupActor, 0.01f, false, RespawnTimer);
	
}

void ATDS_PickupBase::ShowPickupActor()
{
	SetActorHiddenInGame(false);
	SphereCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);

	if (StaticMesh)
	{
		StaticMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
	}
}

bool ATDS_PickupBase::ExecutionEffects(AActor* Player)
{
	return false;
}

void ATDS_PickupBase::OnCompletedEffect()
{
	if (bIsCanRespawn)
	{
		HidePickupActor();
	}
	else
	{
		Destroy();
	}
}