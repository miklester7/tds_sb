// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Types.h"
#include "TDSMainInteractionObject.generated.h"

class USphereComponent;
class USceneComponent;
class USkeletalMeshComponent;
class UStaticMeshComponent;
class UTDSInventoryComponent;

UCLASS()
class TDS_API ATDSMainInteractionObject : public AActor
{
	GENERATED_BODY()
	
public:	
	ATDSMainInteractionObject();

	UPROPERTY(EditAnywhere,BlueprintReadWrite)
		USphereComponent* SphereCollison;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UStaticMeshComponent* StaticMeshSlot;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PickupItems")
		FName ObjectName;

	UFUNCTION(BlueprintCallable)
	void ItemInit(FName ItemName);

	virtual void NotifyActorBeginOverlap(AActor* OtherActor) override;

	FDropItemsInfo CurrentItemInfo;

private:

	FTimerHandle CanTakeTimerHandl;
	void CanTake();

	void SetDepthByType();
	//void Set
};
