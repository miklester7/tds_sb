// Fill out your copyright notice in the Description page of Project Settings.


#include "InteractiongObjects/TDSDoors.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "Components/BoxComponent.h"
#include "Components/SceneComponent.h"
#include "Sound/SoundCue.h"
#include "TimerManager.h"

DEFINE_LOG_CATEGORY_STATIC(DoorLog, All, All);

ATDSDoors::ATDSDoors()
{
	DefaultRootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultRootComponent"));
	SetRootComponent(DefaultRootComponent);

	PrimaryActorTick.bCanEverTick = false;
	Door = CreateDefaultSubobject<UStaticMeshComponent>("Door");
	Door->SetupAttachment(DefaultRootComponent);

	BoxComponent = CreateDefaultSubobject<UBoxComponent>("BoxVolume1");
	BoxComponent->SetupAttachment(DefaultRootComponent);
}

void ATDSDoors::BeginPlay()
{
	Super::BeginPlay();

	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ATDSDoors::DoorInteracting);
	BoxComponent->OnComponentEndOverlap.AddDynamic(this, &ATDSDoors::EndOverlapBox);
}

void ATDSDoors::DoorInteracting(UPrimitiveComponent* OverlappedComponent, AActor*
	OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ActorAround++;

	if (bIsDoorMovement) return;

	if (!bIsOpened)
	{
		//TextComponent1->SetText(FText::FromString(FString("Open")));
		GetWorld()->GetTimerManager().SetTimer(DoorOpenTimerHandle, this, &ATDSDoors::DoorOpening, 0.01, true);
		bIsDoorMovement = true;
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), OpeningDoorSound, GetActorLocation());
	}
}

void ATDSDoors::EndOverlapBox(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	ActorAround--;

	if (ActorAround != 0 || bIsDoorMovement || !bIsOpened) return;

	GetWorld()->GetTimerManager().SetTimer(DoorCloseTimerHandle, this, &ATDSDoors::DoorClosing, 0.01, true);
	bIsDoorMovement = true;
	UGameplayStatics::PlaySoundAtLocation(GetWorld(), ClosingDoorSound, GetActorLocation());
}

void ATDSDoors::DoorOpening()
{
	CurrentLocation = CurrentLocation + FVector(0.f, 0.6f, 0.f);

	Door->SetRelativeLocation(FVector(NULL, CurrentLocation.Y,NULL));

	UE_LOG(DoorLog, Error, TEXT("Current Location %s"), *CurrentLocation.ToString());

	if (CurrentLocation.Y > 120.f)
	{
		GetWorld()->GetTimerManager().ClearTimer(DoorOpenTimerHandle);
		bIsDoorMovement = false;
		bIsOpened = true;
		if (ActorAround == 0)
		{
			GetWorld()->GetTimerManager().SetTimer(DoorCloseTimerHandle, this, &ATDSDoors::DoorClosing, 0.01, true);
			bIsDoorMovement = true;
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), ClosingDoorSound, GetActorLocation());
		}
	}
}

void ATDSDoors::DoorClosing()
{
	CurrentLocation = CurrentLocation + FVector(0.f, -0.6f, 0.f);

	Door->SetRelativeLocation(FVector(NULL, CurrentLocation.Y, NULL));

	UE_LOG(DoorLog, Error, TEXT("Current Location %s"), *CurrentLocation.ToString());

	if (CurrentLocation.Y < 0.f)
	{
		GetWorld()->GetTimerManager().ClearTimer(DoorCloseTimerHandle);
		bIsDoorMovement = false;
		bIsOpened = false;

		if (ActorAround != 0)
		{
			GetWorld()->GetTimerManager().SetTimer(DoorOpenTimerHandle, this, &ATDSDoors::DoorOpening, 0.01, true);
			bIsDoorMovement = true;
			UGameplayStatics::PlaySoundAtLocation(GetWorld(), OpeningDoorSound, GetActorLocation());
		}
	}
}