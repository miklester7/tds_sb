// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/TDS_BaseHealthComponent.h"
#include "TDS_CharacterHealthComponent.generated.h"

DECLARE_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, const float, const float);
DECLARE_MULTICAST_DELEGATE(FOnShieldRegeneration);
DECLARE_MULTICAST_DELEGATE(FOnShieldPenetration);

UCLASS()
class TDS_API UTDS_CharacterHealthComponent : public UTDS_BaseHealthComponent
{
	GENERATED_BODY()
public:
	FOnShieldChange OnShieldCnahge;
	FOnShieldRegeneration OnShieldRegeneration;
	FOnShieldPenetration OnShieldPenetration;

	const float GetShieldPersent() { return CurrentShield / DefaultShield; }
	const float GetShield() { return CurrentShield; }
	const float GetDefaultShield() { return DefaultShield; }
	const float GetCurrentShield() { return CurrentShield; }

	void SetInvulnerability(bool Invulnerability);

	void SetAdditionalShield(float Value);
	float GetAdditionalShieldPercent() { return CurrentShield / (MaxAdditionalShield + DefaultShield); }

protected:
	float MaxAdditionalShield = 0;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Health")
	float DefaultShield = 40.f;

	float CurrentShield = 0.f;

	float AdditionalShield = 0.f;

	UPROPERTY(EditDefaultsOnly,Category = "Health")
	float RegenerationValue = 0.5f;

	UPROPERTY(EditDefaultsOnly, Category = "Health")
	float AddShieldValue = 30.f;

	virtual float TakeAnyDamage(float Damage) override;
	virtual void BeginPlay() override;

	FTimerHandle TakingDamageHandle;
	FTimerHandle ShieldRegenerationHandle;

	FTimerHandle SetAdditionalShieldHandle;

	void AddShield();
	void CanRegeneration();
	void StartTimer();
	void RegenerationShield();

	bool bIsFirstPenetration = true;
};
