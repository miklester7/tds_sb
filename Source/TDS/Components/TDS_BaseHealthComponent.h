// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TDS_BaseHealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);
DECLARE_MULTICAST_DELEGATE_ThreeParams(FOnHealthChange, const float, const float, const bool);
DECLARE_MULTICAST_DELEGATE_ThreeParams(FOnTakeDamage, const float, FVector&, const FLinearColor&);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UTDS_BaseHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTDS_BaseHealthComponent();

	UFUNCTION()
	void TakePointDamage(AActor*DamagedActor,
		float Damage, class AController* InstigatedBy, FVector HitLocation,
		class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection,
		const class UDamageType* DamageType, AActor* DamageCauser);

	UFUNCTION()
	void TakeRadialDamage(AActor* DamagedActor,
		float Damage, const class UDamageType* DamageType,
		FVector Origin, FHitResult HitInfo, class AController* InstigatedBy,
		AActor* DamageCauser);

	virtual float TakeAnyDamage(float Damage);

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Health")
	float DefaultHelth = 100.f;

	float CurrentHealth = 0.f;

	bool bIsInvulnerability = false;

	UPROPERTY(BlueprintAssignable)
	FOnDeath OnDeath;

	FOnTakeDamage TakeDamage;

	FOnHealthChange OnHealthChange;

	const float GetHealthPercent() { return CurrentHealth / DefaultHelth; }
	const float GetHealth() { return CurrentHealth; }
	const float GetDefaultHealth() { return DefaultHelth; }

	bool AddHealth(float AddedHealth);
protected:
	virtual void BeginPlay() override;
		
};
