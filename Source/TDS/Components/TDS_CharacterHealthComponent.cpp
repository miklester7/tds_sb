// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/TDS_CharacterHealthComponent.h"
#include "PlayerCharacter.h"

DEFINE_LOG_CATEGORY_STATIC(CharacterHelthLog, All, All);

void UTDS_CharacterHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	check(DefaultShield >= 0);

	CurrentShield = DefaultShield;

	MaxAdditionalShield = DefaultShield + DefaultHelth;
}

float UTDS_CharacterHealthComponent::TakeAnyDamage(float Damage)
{
	if (bIsInvulnerability)
	{
		return Damage = 0.f;
	}

	GetWorld()->GetTimerManager().ClearTimer(TakingDamageHandle);
	GetWorld()->GetTimerManager().ClearTimer(ShieldRegenerationHandle);

	const float AfterDamage = CurrentShield - Damage;
	if (AfterDamage >= 0)
	{
		Damage = 0;
		CurrentShield = AfterDamage;
	}
	else
	{
		if (bIsFirstPenetration)
		{
			OnShieldPenetration.Broadcast();
		}
		
		bIsFirstPenetration = false;

		CurrentShield = 0;
		Damage = AfterDamage * (-1);
	}
	Super::TakeAnyDamage(Damage);

	OnShieldCnahge.Broadcast(CurrentShield, GetShieldPersent());

	StartTimer();

	return Damage;
}

void UTDS_CharacterHealthComponent::CanRegeneration()
{
	GetWorld()->GetTimerManager().SetTimer(ShieldRegenerationHandle, this, &UTDS_CharacterHealthComponent::RegenerationShield, 0.05f, true, 0.f);
}

void UTDS_CharacterHealthComponent::StartTimer()
{
	GetWorld()->GetTimerManager().SetTimer(TakingDamageHandle, this, &UTDS_CharacterHealthComponent::CanRegeneration, 0.01f, false, 3.f);
}

void UTDS_CharacterHealthComponent::RegenerationShield()
{
	if (CurrentShield >= DefaultShield)
	{
		GetWorld()->GetTimerManager().ClearTimer(ShieldRegenerationHandle);
		return;
	}

	if (CurrentShield == 0)
	{
		OnShieldRegeneration.Broadcast();
		bIsFirstPenetration = true;
	}

	CurrentShield = FMath::Clamp(CurrentShield + RegenerationValue, 0, DefaultShield);

	OnShieldCnahge.Broadcast(CurrentShield, GetShieldPersent());
}

void UTDS_CharacterHealthComponent::SetInvulnerability(bool Invulnerability)
{
	bIsInvulnerability = Invulnerability;
}

void UTDS_CharacterHealthComponent::SetAdditionalShield(float Value)
{
	AdditionalShield = FMath::Clamp(AdditionalShield + Value, 0, MaxAdditionalShield);

	GetWorld()->GetTimerManager().SetTimer(SetAdditionalShieldHandle, this, &UTDS_CharacterHealthComponent::AddShield, 0.05f, true);
}

void UTDS_CharacterHealthComponent::AddShield()
{
	CurrentShield = FMath::Clamp(CurrentShield + AddShieldValue, 0, MaxAdditionalShield + DefaultShield);
	OnShieldCnahge.Broadcast(CurrentShield, GetShieldPersent());

	AdditionalShield = AdditionalShield - AddShieldValue;

	if (AdditionalShield <= 0 || CurrentShield == MaxAdditionalShield + DefaultShield)
	{
		AdditionalShield = 0;
		GetWorld()->GetTimerManager().ClearTimer(SetAdditionalShieldHandle);
	}

}