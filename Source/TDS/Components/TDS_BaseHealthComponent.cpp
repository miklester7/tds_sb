// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/TDS_BaseHealthComponent.h"
#include "TDSGameInstance.h"
#include "TDSUtils.h"
#include "PlayerCharacter.h"
#include "Math/UnrealMathUtility.h"
#include "Weapon/DamageType/TDS_CritDamageType.h"
#include "Weapon/DamageType/TDS_LightningType.h"

DEFINE_LOG_CATEGORY_STATIC(BaseHelthLog, All, All);

UTDS_BaseHealthComponent::UTDS_BaseHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UTDS_BaseHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	if (DefaultHelth <= 0) DefaultHelth = 1;

	CurrentHealth = DefaultHelth;

	AActor* CurrentOwner = GetOwner();
	check(CurrentOwner);
	CurrentOwner->OnTakePointDamage.AddDynamic(this, &UTDS_BaseHealthComponent::TakePointDamage);
	CurrentOwner->OnTakeRadialDamage.AddDynamic(this, &UTDS_BaseHealthComponent::TakeRadialDamage);
}

void UTDS_BaseHealthComponent::TakePointDamage(AActor* DamagedActor,
	float Damage, class AController* InstigatedBy, FVector HitLocation,
	class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection,
	const class UDamageType* DamageType, AActor* DamageCauser)
{
	auto DamageTypeClass = Cast<UTDS_CritDamageType>(DamageType);
	FLinearColor DamageColor;
	float DamageDone = TakeAnyDamage(Damage);

	if (DamageDone == 0)
	{
		DamageColor = FLinearColor::Blue;
	}
	else if (DamageTypeClass)
	{
		DamageColor = FLinearColor::Yellow;
	}
	else
	{
		DamageColor = FLinearColor::White;
	}

	if (bIsInvulnerability) Damage = 0;

	TakeDamage.Broadcast(Damage, HitLocation, DamageColor);
}

void UTDS_BaseHealthComponent::TakeRadialDamage(AActor* DamagedActor,
	float Damage, const class UDamageType* DamageType,
	FVector Origin, FHitResult HitInfo, class AController* InstigatedBy,
	AActor* DamageCauser)
{
	float DamageDone = TakeAnyDamage(Damage);
	FVector ZeroVector = FVector::ZeroVector;

	FLinearColor DamageColor = FLinearColor::Red;

	const auto Type = Cast<UTDS_LightningType>(DamageType);
	if (Type)
	{
		DamageColor = FLinearColor::Gray;
	}
	else if(DamageDone == 0)
	{
		DamageColor = FLinearColor::Blue;
	}

	if (bIsInvulnerability) Damage = 0;
	
	TakeDamage.Broadcast(Damage, ZeroVector, DamageColor);
}

float UTDS_BaseHealthComponent::TakeAnyDamage(float Damage)
{
	CurrentHealth = FMath::Clamp(CurrentHealth - Damage, 0, DefaultHelth);

	const auto CurrentOwner = GetOwner<APlayerCharacter>();

	if (CurrentOwner)
	{
		if(Damage > 0) OnHealthChange.Broadcast(CurrentHealth, GetHealthPercent(),true);
	}

	if (CurrentHealth == 0)
	{
		OnDeath.Broadcast();
	}

	return Damage;
}

bool UTDS_BaseHealthComponent::AddHealth(float AddedHealth)
{
	if (CurrentHealth < DefaultHelth)
	{
		CurrentHealth = FMath::Clamp(CurrentHealth + AddedHealth, 0, DefaultHelth);

		OnHealthChange.Broadcast(CurrentHealth, GetHealthPercent(),false);
		return true;
	}
	return false;
}

