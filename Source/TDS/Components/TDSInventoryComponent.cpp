// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/TDSInventoryComponent.h"
#include "Engine/World.h"
#include "PlayerCharacter.h"
#include "Weapon/TDSBaseWeapon.h"
#include "TDSGameInstance.h"
#include "Math/UnrealMathUtility.h"


DEFINE_LOG_CATEGORY_STATIC(InventoryLog, All, All);

UTDSInventoryComponent::UTDSInventoryComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

}

void UTDSInventoryComponent::CleaningLines()
{
	check(InventoryWeaponNames.Num() <= 3);//the number of weapon slots must not exceed the specified parameter.

	if (!InventoryWeaponNames.IsValidIndex(0)) return;

	const auto Instance = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (Instance)
	{
		TArray<FName> AvailableNames;
		if (!Instance->GetAllWeaponRowNames(AvailableNames)) return;

		for (FName WeaponName : InventoryWeaponNames)
		{
			bool bIsContains = AvailableNames.Contains(WeaponName);

			if (!bIsContains)
			{
				UE_LOG(InventoryLog, Error, TEXT("Not contains %s"), *WeaponName.ToString());

				int32 Index = InventoryWeaponNames.Find(WeaponName);

				InventoryWeaponNames[Index] = "None";
			}
		}
	}
	FindMatches();

	InventoryWeaponNames.Remove("None");

	CheckForMatches();
	SetAmmoSlotTypesForWeapon(true);
}

void UTDSInventoryComponent::CheckForMatches()
{
	for (int32 i = 0; i < InventoryAmmoData.Num(); i++)
	{
		for (int32 j = 0; j < InventoryAmmoData.Num(); j++)
		{
			if (i == j) continue;
			check(InventoryAmmoData[i].AmmoType == InventoryAmmoData[j].AmmoType);//The ammunition array should not contain the same types!
		}
	}
}

void UTDSInventoryComponent::FindMatches()
{
	const auto Instance = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (!Instance) return;

	FWeaponInfo FirstInfo, SecondInfo;

	for (int32 i = 0; i < InventoryWeaponNames.Num(); i++)
	{
		if (InventoryWeaponNames[i] == "None") continue;

		if (Instance->GetWeaponInfoByName(InventoryWeaponNames[i], FirstInfo))
		{
			for (int32 j = 0; j < InventoryWeaponNames.Num(); j++)
			{
				if (i == j) continue;

				if (Instance->GetWeaponInfoByName(InventoryWeaponNames[j], SecondInfo))
				{
					if (SecondInfo.WeaponType == FirstInfo.WeaponType)
					{
						UE_LOG(InventoryLog, Error, TEXT("Duplicate types found! %s was deleted"), *InventoryWeaponNames[j].ToString());
						InventoryWeaponNames[j] = "None";
					}
				}
			}
		}
	}
	
}


bool UTDSInventoryComponent::GetFirstSlot(FName& WeaponName)
{
	if (!InventoryWeaponNames.IsValidIndex(0)) return false;

	WeaponName = InventoryWeaponNames[0];

	return true;
}

bool UTDSInventoryComponent::FindAmmunition(int32& AllAmmo, EWeaponType CurrentAmmoType, bool IsIndex)
{
	for (int8 i = 0; i < InventoryAmmoData.Num(); ++i)
	{
		if (InventoryAmmoData[i].AmmoType == CurrentAmmoType)
		{
			if (IsIndex)
			{
				AllAmmo = i;
				return true;
			}
			else
			{
				AllAmmo = InventoryAmmoData[i].AmmoCount;
				return true;
			}
		
		}
	}

	return false;
}

bool UTDSInventoryComponent::SetWeaponAmmoForType(EWeaponType WeaponType)
{
	int32 Index, AddAmmo;
	if (FindAmmunition(Index,WeaponType,true))
	{
		switch (WeaponType)
		{
		case EWeaponType::Rifle_Type:
			AddAmmo = 30;
			break;
		case EWeaponType::Sniper_Type:
			AddAmmo = 5;
			break;
		case EWeaponType::Shotgun_Type:
			AddAmmo = 8;
			break;
		case EWeaponType::Launcher_Type:
			AddAmmo = 1;
			break;
		default:
			AddAmmo = 0;
			break;
		}
		if (InventoryAmmoData[Index].AmmoCount == AddAmmo * 10) return false;

		int32 NewAmmoCount = FMath::Clamp(InventoryAmmoData[Index].AmmoCount + AddAmmo, 0, AddAmmo * 10);
		InventoryAmmoData[Index].AmmoCount = NewAmmoCount;

		return true;
	}

	return false;
}

bool UTDSInventoryComponent::GetWeaponAmmoForType(EWeaponType WeaponType,int32& AmmoCount, int32 AmmoFromInventory)
{
	int32 Index;
	if (FindAmmunition(Index, WeaponType, true))
	{
		if (InventoryAmmoData[Index].AmmoCount == 0)
		{
			return false;
		}

		if (InventoryAmmoData[Index].AmmoCount < AmmoFromInventory)
		{
			AmmoCount = InventoryAmmoData[Index].AmmoCount;
			InventoryAmmoData[Index].AmmoCount = 0;
			return true;
		}
		else
		{
			AmmoCount = InventoryAmmoData[Index].AmmoCount -= AmmoFromInventory;
			return true;
		}
	}
	return false;
}

void UTDSInventoryComponent::SetAmmoSlotTypesForWeapon(bool bIsFirst)
{
	const auto Instance = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (!Instance) return;

	FWeaponInfo WeaponInfo;

	for (FName WeaponName : InventoryWeaponNames)
	{
		if (Instance->GetWeaponInfoByName(WeaponName, WeaponInfo))
		{
			bool bIsContains = false;

			for (int32 i = 0; i < InventoryAmmoData.Num();i++)
			{
				if (InventoryAmmoData[i].AmmoType == WeaponInfo.WeaponType)
				{
					bIsContains = true;

					if (!bIsFirst) return;

					if (WeaponInfo.Infinite)
					{
						InventoryAmmoData[i].AmmoInClip = WeaponInfo.DefaultBullets;
					}
					else if(InventoryAmmoData[i].AmmoCount > WeaponInfo.DefaultBullets)
					{
						InventoryAmmoData[i].AmmoInClip = WeaponInfo.DefaultBullets;
						InventoryAmmoData[i].AmmoCount -= WeaponInfo.DefaultBullets;
					}
					else
					{
						InventoryAmmoData[i].AmmoInClip = InventoryAmmoData[i].AmmoCount;
						InventoryAmmoData[i].AmmoCount = 0;
					}
					
				}
			}

			if (!bIsContains)
			{
				UE_LOG(InventoryLog, Warning, TEXT("Ammo slot added for %s"), *WeaponName.ToString());
				FAmmoData NewAmmo;
				NewAmmo.AmmoType = WeaponInfo.WeaponType;
				InventoryAmmoData.Add(NewAmmo);
			}
		}
	}
}

int32 UTDSInventoryComponent::DropWeaponFromInventory(FName DropWeaponName)
{
	int32 Index = -1;
	Index = InventoryWeaponNames.Find(DropWeaponName);
	InventoryWeaponNames.RemoveAt(Index);

	return Index;
}

bool UTDSInventoryComponent::TryGetNewWeapon(FName Name)
{
	const auto Instance = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (!Instance) return false;
	
	FWeaponInfo Info;
	Instance->GetWeaponInfoByName(Name, Info);

	EWeaponType NewWeaponType = Info.WeaponType;

	for (FName WeaponName : InventoryWeaponNames)
	{
		if (WeaponName == Name) return false;

		Instance->GetWeaponInfoByName(WeaponName, Info);

		if (Info.WeaponType == NewWeaponType) return false;
	}

	InventoryWeaponNames.Add(Name);
	SetAmmoSlotTypesForWeapon();

	return true;
}

void UTDSInventoryComponent::SetAmmoInClipForType(EWeaponType WeaponType, int32 AmmoForClip)
{
	for (int32 i = 0; i < InventoryAmmoData.Num(); ++i)
	{
		if (InventoryAmmoData[i].AmmoType == WeaponType)
		{
			InventoryAmmoData[i].AmmoInClip = AmmoForClip;
			return;
		}
	}
}

bool UTDSInventoryComponent::TryGetLoadedClip(EWeaponType WeaponType, int32& AmmoForClip)
{
	for (int32 i = 0; i < InventoryAmmoData.Num(); ++i)
	{
		if (InventoryAmmoData[i].AmmoType == WeaponType)
		{
			AmmoForClip = InventoryAmmoData[i].AmmoInClip;
			return true;
		}
	}

	return false;
}

bool UTDSInventoryComponent::TryGetWeaponFromSlot(int32 InventorySlot, FName& WeaponName)
{
	if (InventoryWeaponNames.IsValidIndex(InventorySlot))
	{
		WeaponName = InventoryWeaponNames[InventorySlot];
		return true;
	}

	return false;
}

int32 UTDSInventoryComponent::GetWeaponIndexByName(FName WeaponName)
{
	return  InventoryWeaponNames.Find(WeaponName);
}