// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Types.h"
#include "TDSInventoryComponent.generated.h"

class UTDSGameInstance;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TDS_API UTDSInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTDSInventoryComponent();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		TArray<FAmmoData> InventoryAmmoData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		TArray<FName> InventoryWeaponNames;

	bool GetFirstSlot(FName& WeaponName);
	int32 GetWeaponIndexByName(FName WeaponName);

	bool FindAmmunition(int32& AllAmmo, EWeaponType CurrentAmmoType,bool IsIndex);

	bool SetWeaponAmmoForType(EWeaponType WeaponType);
	bool GetWeaponAmmoForType(EWeaponType WeaponType,int32& AmmoCount,int32 AmmoFromInventory);//Use for reload

	void CleaningLines();
	void CheckForMatches();
	void FindMatches();

	int32 DropWeaponFromInventory(FName DropWeaponName);
	bool TryGetNewWeapon(FName Name);

	void SetAmmoInClipForType(EWeaponType WeaponType,int32 AmmoForClip);

	bool TryGetLoadedClip(EWeaponType WeaponType, int32& AmmoForClip);

	bool TryGetWeaponFromSlot(int32 InventorySlot,FName& WeaponName);
private:
	void SetAmmoSlotTypesForWeapon(bool bIsFirst = false);
};
