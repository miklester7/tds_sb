// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/TDS_GDamageActor.h"
#include "Components/WidgetComponent.h"
#include "UI/TDS_DamageWidget.h"

ATDS_GDamageActor::ATDS_GDamageActor()
{
	PrimaryActorTick.bCanEverTick = false;

	WidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("WidgetComponent"));
	WidgetComponent->SetWidgetSpace(EWidgetSpace::Screen);
	SetRootComponent(WidgetComponent);
}

void ATDS_GDamageActor::BeginPlay()
{
	Super::BeginPlay();
	
	SetLifeSpan(0.5f);
}

void ATDS_GDamageActor::SetWidgetInfo(const float Value,const FLinearColor& Color)
{
	auto Damagewidget = Cast<UTDS_DamageWidget>(WidgetComponent->GetWidget());
	if (!Damagewidget) return;

	Damagewidget->SetTextDamage(Value, Color);
}


