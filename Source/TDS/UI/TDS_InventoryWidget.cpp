// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/TDS_InventoryWidget.h"
#include "Components/VerticalBox.h"
#include "UI/TDS_InventoryRowsWidget.h"
#include "Components/TDSInventoryComponent.h"
#include "Components/Border.h"
#include "TDSGameInstance.h"
#include "TDSUtils.h"

DEFINE_LOG_CATEGORY_STATIC(TestinventoryWidgetLog, All, All);

void UTDS_InventoryWidget::NativeConstruct()
{
	Super::NativeConstruct();

	InitVerticvalBox();
}

void UTDS_InventoryWidget::InitVerticvalBox()
{
	if (!InventoryVerticalBox) return;

	InventoryVerticalBox->ClearChildren();

	if (!RowWidget) return;

	const auto Player = GetOwningPlayerPawn();
	if (!Player) return;

	const auto inventoryComponent = TDSUtils::GetTDSComponentByClass<UTDSInventoryComponent>(Player);

	auto myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());

	if (!inventoryComponent || !myGI) return;

	FDropItemsInfo Info;

	for (FName WeaponName : inventoryComponent->InventoryWeaponNames)
	{
		const auto NewWidget = CreateWidget<UTDS_InventoryRowsWidget>(this, RowWidget);
		if (!NewWidget) return;

		RowWidgets.Add(NewWidget);

		myGI->GetItemsInfoByName(WeaponName, Info);

		NewWidget->SetInfoWeaponName(WeaponName, Info.InventoryImage);

		InventoryVerticalBox->AddChild(NewWidget);
	}
}

void UTDS_InventoryWidget::DestroyRowWodgetForIndex(int32 Index)
{
	if (RowWidgets.IsValidIndex(Index))
	{
		RowWidgets[Index]->HideStatsWidget();
		RowWidgets[Index]->RemoveFromParent();
		RowWidgets.RemoveAt(Index);
	}
}

void UTDS_InventoryWidget::AddRowWidget(FName NewWeapon)
{
	auto myGI = Cast<UTDSGameInstance>(GetWorld()->GetGameInstance());
	if (!myGI) return;

	const auto NewWidget = CreateWidget<UTDS_InventoryRowsWidget>(this, RowWidget);
	if (!NewWidget) return;

	FDropItemsInfo Info;

	RowWidgets.Add(NewWidget);

	myGI->GetItemsInfoByName(NewWeapon, Info);

	NewWidget->SetInfoWeaponName(NewWeapon, Info.InventoryImage);

	InventoryVerticalBox->AddChild(NewWidget);
}