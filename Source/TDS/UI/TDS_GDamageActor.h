// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDS_GDamageActor.generated.h"

class UWidgetComponent;
class UTDS_DamageWidget;

UCLASS()
class TDS_API ATDS_GDamageActor : public AActor
{
	GENERATED_BODY()
	
public:	
	ATDS_GDamageActor();

	UPROPERTY(VisibleAnywhere)
	UWidgetComponent* WidgetComponent;

	void SetWidgetInfo(const float Value,const FLinearColor& Color);

protected:
	virtual void BeginPlay() override;
};
