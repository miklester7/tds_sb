// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/MenuWidgets/SettingOptionWidget.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "Settings/TDS_GameSetting.h"

void USettingOptionWidget::NativeConstruct()
{
	Super::NativeConstruct();

	check(SettingDisplayName);
	check(SettingCurrentValue);
	check(NextOptionButton);
	check(PrevOptionButton);

	NextOptionButton->OnClicked.AddDynamic(this, &USettingOptionWidget::OnNextSetting);
	PrevOptionButton->OnClicked.AddDynamic(this, &USettingOptionWidget::OnPrevSetting);
}
void USettingOptionWidget::Init(UTDS_GameSetting* InSetting)
{
	Setting = MakeWeakObjectPtr(InSetting);
	check(Setting.IsValid());

	UpdateText();
}

void USettingOptionWidget::UpdateText()
{
	if (Setting.IsValid())
	{
		SettingDisplayName->SetText(Setting->GetName());
		SettingCurrentValue->SetText(Setting->GetCurrentOption().Name);
	}
}

void USettingOptionWidget::OnNextSetting()
{
	if (Setting.IsValid())
	{
		Setting->ApplyNextOption();
		SettingCurrentValue->SetText(Setting->GetCurrentOption().Name);
	}
}

void USettingOptionWidget::OnPrevSetting()
{
	if (Setting.IsValid())
	{
		Setting->ApplyPrevOption();
		SettingCurrentValue->SetText(Setting->GetCurrentOption().Name);
	}
}
