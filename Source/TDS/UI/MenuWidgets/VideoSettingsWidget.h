// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "VideoSettingsWidget.generated.h"

class UVerticalBox;
class USettingOptionWidget;
class UButton;

UCLASS()
class TDS_API UVideoSettingsWidget : public UUserWidget
{
	GENERATED_BODY()

protected:

	UPROPERTY(meta = (BindWidget))
	UVerticalBox* VideoSettingsContainer;

	UPROPERTY(meta = (BindWidget))
	UButton* RunBenchmarkButton;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "UI")
	TSubclassOf<USettingOptionWidget> SettingOptionWidget;

	virtual void NativeConstruct() override;

private:
	UFUNCTION()
	void OnBenchmark();

	void OnVideoSettingsUpdated();
};
