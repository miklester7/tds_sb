// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/MenuWidgets/VideoSettingsWidget.h"
#include "UI/MenuWidgets/SettingOptionWidget.h"
#include "Settings/TDS_GameUserSettings.h"
#include "Components/VerticalBox.h"
#include "Components/Button.h"
#include "Settings/TDS_GameSetting.h"

DEFINE_LOG_CATEGORY_STATIC(LogVideoSettingsWidget, All, All);

void UVideoSettingsWidget::NativeConstruct()
{
	Super::NativeConstruct();

	auto* UserSettings = UTDS_GameUserSettings::Get();
	if (!UserSettings)
	{
		UE_LOG(LogVideoSettingsWidget, Error, TEXT("UserSettings is nullptr"));
		return;
	}

	UserSettings->LoadSettings();

	const auto VideoSettings = UserSettings->GetVideoSettings();

	check(VideoSettingsContainer);

	VideoSettingsContainer->ClearChildren();

	for (auto* Setting : VideoSettings)
	{
		const auto SettingWidget = CreateWidget<USettingOptionWidget>(this, SettingOptionWidget);
		check(SettingWidget);

		SettingWidget->Init(Setting);

		VideoSettingsContainer->AddChild(SettingWidget);
		UE_LOG(LogVideoSettingsWidget, Error, TEXT("Widget added"));
	}

	check(RunBenchmarkButton);

	RunBenchmarkButton->OnClicked.AddDynamic(this, &UVideoSettingsWidget::OnBenchmark);

	UserSettings->OnVideoSettingsUpdated.AddUObject(this, &UVideoSettingsWidget::OnVideoSettingsUpdated);
}

void UVideoSettingsWidget::OnBenchmark()
{
	if (auto* UserSettings = UTDS_GameUserSettings::Get())
	{
		UserSettings->RunBenchmark();
	}
}

void UVideoSettingsWidget::OnVideoSettingsUpdated()
{
	if (!VideoSettingsContainer) return;

	for (auto* Widget : VideoSettingsContainer->GetAllChildren())
	{
		if (auto* OptionWidget = Cast<USettingOptionWidget>(Widget))
		{
			OptionWidget->UpdateText();
		}
	}
}
