// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "SettingOptionWidget.generated.h"

class UTextBlock;
class UTDS_GameSetting;
class UButton;

UCLASS()
class TDS_API USettingOptionWidget : public UUserWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	UTextBlock* SettingDisplayName;

	UPROPERTY(meta = (BindWidget))
	UTextBlock* SettingCurrentValue;

	UPROPERTY(meta = (BindWidget))
	UButton* NextOptionButton;

	UPROPERTY(meta = (BindWidget))
	UButton* PrevOptionButton;
	
	virtual void NativeConstruct() override;
private:
	TWeakObjectPtr<UTDS_GameSetting> Setting;

	void Init(UTDS_GameSetting* Setting);

	void UpdateText();

	UFUNCTION()
	void OnNextSetting();

	UFUNCTION()
	void OnPrevSetting();

	friend class UVideoSettingsWidget;

};
