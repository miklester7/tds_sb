// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Types.h"
#include "TDS_InventoryWeaponStatWidget.generated.h"

class UTextBlock;
class UImage;

UCLASS()
class TDS_API UTDS_InventoryWeaponStatWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void SetWidgetStats(FWeaponInfo& WeaponStats, FName WeaponName);

private:

	UPROPERTY(meta = (BindWidget))
		UTextBlock* WeaponNameText;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* WeaponDamage;
};
