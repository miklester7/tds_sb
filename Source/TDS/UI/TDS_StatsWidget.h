// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDS_StatsWidget.generated.h"

class USizeBox;
class UProgressBar;
class UTextBlock;
class UWidgetAnimation;

UCLASS()
class TDS_API UTDS_StatsWidget : public UUserWidget
{
	GENERATED_BODY()
	
private:
	UPROPERTY(meta = (BindWidget))
		USizeBox* HealthSizeBox;

	UPROPERTY(meta = (BindWidget))
		USizeBox* ShieldSizeBox;

	UPROPERTY(meta = (BindWidget))
		USizeBox* BaseSizeBox;

	UPROPERTY(meta = (BindWidget))
		UProgressBar* HealthProgressBar;

	UPROPERTY(meta = (BindWidget))
		UProgressBar* ShieldProgressBar;

	UPROPERTY(meta = (BindWidget))
		UProgressBar* StaminaProgressBar;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* HealthTextBlock;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* ShieldTextBlock;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
		UWidgetAnimation* LowStaminaAnimation;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
		UWidgetAnimation* EnlargedShieldAnimation;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
		UWidgetAnimation* DefaultShieldAnimation;

	bool bIsActivAdditionalShield = false;
public:
	void ChangeRatio(float RatioHealthToShield);
	void SetHelth(float Health,float NormalizeHealth);
	void SetShield(float Shield, float NormalizeShield);
	void SetStamina(float NormalizeStamina);
	void ChangeShieldColor(bool IsEnlargedShield);

	void SetAdditionalShieldAndRation(float Shield, float RatioHealthToShield);
};
