// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/TDS_StatsWidget.h"
#include "Components/SizeBox.h"
#include "Components/ProgressBar.h"
#include "Components/TextBlock.h"

void UTDS_StatsWidget::SetHelth(float Health, float NormalizeHealth)
{
	if (!HealthProgressBar || !HealthTextBlock) return;

	const int32 CurrentHealth = static_cast<int32>(Health);
	
	HealthProgressBar->SetPercent(NormalizeHealth);
	HealthTextBlock->SetText(FText::FromString(FString::FromInt(CurrentHealth)));

}

void UTDS_StatsWidget::SetShield(float Shield, float NormalizeShield)
{
	if (!ShieldProgressBar || !ShieldTextBlock) return;

	if (bIsActivAdditionalShield)
	{
		bIsActivAdditionalShield = false;
		ChangeShieldColor(false);
	}

	const int32 CurrentShield = static_cast<int32>(Shield);

	ShieldProgressBar->SetPercent(NormalizeShield);
	ShieldTextBlock->SetText(FText::FromString(FString::FromInt(CurrentShield)));
}

void UTDS_StatsWidget::ChangeRatio(float RatioHealthToShield)
{
	if (!BaseSizeBox || !HealthSizeBox || !ShieldSizeBox) return;

	float BaseSize = BaseSizeBox->GetWidthOverride();

	float WidthHelthBar = BaseSize * RatioHealthToShield;
	
	HealthSizeBox->SetWidthOverride(WidthHelthBar);
	ShieldSizeBox->SetWidthOverride(BaseSize - WidthHelthBar);
}

void UTDS_StatsWidget::SetStamina(float NormalizeStamina)
{
	if (!StaminaProgressBar) return;

	StaminaProgressBar->SetPercent(NormalizeStamina);

	if (NormalizeStamina < 0.15f)
	{
		if(!IsAnimationPlaying(LowStaminaAnimation)) PlayAnimation(LowStaminaAnimation);	
	}
	else
	{
		if (IsAnimationPlaying(LowStaminaAnimation)) StopAnimation(LowStaminaAnimation);
	}
}

void UTDS_StatsWidget::SetAdditionalShieldAndRation(float Shield, float RatioHealthToShield)
{
	if (!bIsActivAdditionalShield)
	{
		bIsActivAdditionalShield = true;
		ChangeShieldColor(true);
		ShieldProgressBar->SetPercent(1);
	}

	const int32 CurrentShield = static_cast<int32>(Shield);

	ShieldTextBlock->SetText(FText::FromString(FString::FromInt(CurrentShield)));

	float BaseSize = BaseSizeBox->GetWidthOverride();

	float WidthHelthBar = BaseSize * RatioHealthToShield;

	HealthSizeBox->SetWidthOverride(WidthHelthBar);
	ShieldSizeBox->SetWidthOverride(BaseSize - WidthHelthBar);
}

void UTDS_StatsWidget::ChangeShieldColor(bool IsEnlargedShield)
{
	if (!ShieldProgressBar) return;

	if (IsEnlargedShield)
	{
		if (!IsAnimationPlaying(EnlargedShieldAnimation)) PlayAnimation(EnlargedShieldAnimation);
	}
	else
	{
		if (!IsAnimationPlaying(DefaultShieldAnimation)) PlayAnimation(DefaultShieldAnimation);
	}
}
