// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDS_InventoryWidget.generated.h"

class UVerticalBox;
class UTDS_InventoryRowsWidget;
class UBorder;

UCLASS()
class TDS_API UTDS_InventoryWidget : public UUserWidget
{
	GENERATED_BODY()
public:
	void InitVerticvalBox();

	virtual void NativeConstruct() override;

	void DestroyRowWodgetForIndex(int32 Index);

	void AddRowWidget(FName NewWeapon);

	UPROPERTY(meta = (BindWidget))
		UBorder* WidgetBorder;
private:
	UPROPERTY(meta = (BindWidget))
		UVerticalBox* InventoryVerticalBox;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<UTDS_InventoryRowsWidget> RowWidget;

	TArray<UTDS_InventoryRowsWidget*> RowWidgets;
};
