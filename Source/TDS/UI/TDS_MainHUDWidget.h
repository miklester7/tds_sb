// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDS_MainHUDWidget.generated.h"

class UImage;
class UImageBlock;
class UTextBlock;
class UBackgroundBlur;

UCLASS()
class TDS_API UTDS_MainHUDWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:

	void SetAmmoClip(int32 Ammo);
	void SetAllAmmo(int32 Ammo);
	void SetWeaponImage(UTexture2D* Image);

	UPROPERTY(meta = (BindWidget))
		UBackgroundBlur* StatsBlur;

private:

	UPROPERTY(meta = (BindWidget))
		UImage* WeaponImage;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* TextAmmoInClip;

	UPROPERTY(meta = (BindWidget))
		UTextBlock* TextAllAmmo;
};
