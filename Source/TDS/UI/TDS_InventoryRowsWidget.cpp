// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/TDS_InventoryRowsWidget.h"
#include "UI/TDS_InventoryWidget.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "UI/TDS_InventoryWeaponStatWidget.h"
#include "TDSGameInstance.h"
#include "Components/Image.h"
#include "TDSPlayerController.h"
#include "Types.h"


DEFINE_LOG_CATEGORY_STATIC(TestWidgetLog, All, All);

void UTDS_InventoryRowsWidget::NativeOnInitialized()
{
	WeaponPickupButton->OnHovered.AddDynamic(this, &UTDS_InventoryRowsWidget::ShowStatsWidget);
	WeaponPickupButton->OnUnhovered.AddDynamic(this, &UTDS_InventoryRowsWidget::HideStatsWidget);
	WeaponPickupButton->OnPressed.AddDynamic(this, &UTDS_InventoryRowsWidget::SetWeapon);
}

void UTDS_InventoryRowsWidget::SetInfoWeaponName(FName NewText, UTexture2D* Image)
{
	if (!WeaponNameTextBlock) return;

	WeaponNameTextBlock->SetText(FText::FromString(NewText.ToString()));

	WeaponSlotName = NewText;

	if (!WeaponImage) return;

	WeaponImage->SetBrushFromTexture(Image);

}

void UTDS_InventoryRowsWidget::ShowStatsWidget()
{
	ATDSPlayerController* Controller = GetOwningPlayer<ATDSPlayerController>();
	if (!Controller) return;

	Controller->CreateInventoryStatsWidget(WeaponSlotName);
}

void UTDS_InventoryRowsWidget::HideStatsWidget()
{
	ATDSPlayerController* Controller = GetOwningPlayer<ATDSPlayerController>();
	if (!Controller) return;

	Controller->DestroyInventoryWidget();
}

void UTDS_InventoryRowsWidget::SetWeapon()
{
	ATDSPlayerController* Controller = GetOwningPlayer<ATDSPlayerController>();
	if (!Controller) return;

	Controller->ChangeWeaponSlot(WeaponSlotName);
}

