// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/TDS_MainHUDWidget.h"
#include "Components/Image.h"
#include "Components/TextBlock.h"
#include "Components/BackgroundBlur.h"

void UTDS_MainHUDWidget::SetAmmoClip(int32 Ammo)
{
	if (!TextAmmoInClip) return;

	TextAmmoInClip->SetText(FText::FromString(FString::FromInt(Ammo)));

}

void UTDS_MainHUDWidget::SetAllAmmo(int32 Ammo)
{
	if (!TextAllAmmo) return;

	TextAllAmmo->SetText(FText::FromString(FString::FromInt(Ammo)));
}

void UTDS_MainHUDWidget::SetWeaponImage(UTexture2D* Image)
{
	if (!WeaponImage) return;

	WeaponImage->SetBrushFromTexture(Image);
}