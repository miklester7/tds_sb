// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDS_DamageWidget.generated.h"

class UTextBlock;

UCLASS()
class TDS_API UTDS_DamageWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	UFUNCTION(BlueprintCallable)
	void SetTextDamage(const float Damage,const FLinearColor& Color);

protected:
	UPROPERTY(meta =(BindWidget))
	UTextBlock* DamageText;
};
