// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "TDS_InventoryRowsWidget.generated.h"

class UTextBlock;
class UButton;
class UImage;
class UTDS_InventoryWeaponStatWidget;

UCLASS()
class TDS_API UTDS_InventoryRowsWidget : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void SetInfoWeaponName(FName NewText, UTexture2D* Image);

	virtual void NativeOnInitialized() override;

	UFUNCTION()
		void ShowStatsWidget();

	UFUNCTION()
		void HideStatsWidget();

	UFUNCTION()
		void SetWeapon();

private:

	UPROPERTY(meta = (BindWidget))
		UTextBlock* WeaponNameTextBlock;

	UPROPERTY(meta = (BindWidget))
		UButton* WeaponPickupButton;

	UPROPERTY(meta = (BindWidget))
		UImage* WeaponImage;

	FName WeaponSlotName;
};
