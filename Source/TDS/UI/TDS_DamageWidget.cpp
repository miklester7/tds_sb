// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/TDS_DamageWidget.h"
#include "Components/TextBlock.h"
#include "Styling/SlateColor.h"

void UTDS_DamageWidget::SetTextDamage(const float Damage, const FLinearColor& Color)
{
	const int32 IntDamage = static_cast<int32>(Damage);
	DamageText->SetText(FText::FromString(FString::FromInt(IntDamage)));
	
	DamageText->SetColorAndOpacity(FSlateColor(Color));
}