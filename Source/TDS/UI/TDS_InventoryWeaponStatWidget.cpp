// Fill out your copyright notice in the Description page of Project Settings.


#include "UI/TDS_InventoryWeaponStatWidget.h"
#include "Components/TextBlock.h"
#include "Components/Image.h"


void UTDS_InventoryWeaponStatWidget::SetWidgetStats(FWeaponInfo& WeaponStats, FName WeaponName)
{
	WeaponNameText->SetText(FText::FromString(WeaponName.ToString()));
	WeaponDamage->SetText(FText::FromString(FString::FromInt(WeaponStats.WeaponDamage)));
}