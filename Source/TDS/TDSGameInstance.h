// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "Types.h"
#include "Engine/DataTable.h"
#include "Weapon/TDSBaseWeapon.h"
#include "UI/TDS_GDamageActor.h"
#include "UI/TDS_InventoryWidget.h"
#include "UI/TDS_InventoryWeaponStatWidget.h"
#include "UI/TDS_DamageWidget.h"

#include "TDSGameInstance.generated.h"

class UMaterialParameterCollection;

UCLASS()
class TDS_API UTDSGameInstance : public UGameInstance
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditDefaultsOnly)
		UMaterialParameterCollection* UIParametrCollection;
public:

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "WeaponSettings")
		UDataTable* WeaponInfoTable = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Items")
		UDataTable* PickupItemsTable = nullptr;

	UFUNCTION(BlueprintCallable)
		bool GetWeaponInfoByName(FName WeaponName, FWeaponInfo& OutInfo);

	UFUNCTION(BlueprintCallable)
		bool GetItemsInfoByName(FName ItemsName, FDropItemsInfo& OutInfo);

	UFUNCTION(BlueprintCAllable)
		bool GetAllWeaponRowNames(TArray<FName>& RowNames);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
		UMaterialInterface* CircleMenuMaterial;

	UMaterialInterface* GetUIMaterial()
	{
		return CircleMenuMaterial;
	}

	UMaterialParameterCollection* GetParametrCollection()
	{
		return UIParametrCollection;
	}

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<UTDS_InventoryWidget> InventoryWidget;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<UTDS_InventoryWeaponStatWidget> WeaponStatsWidget;

};
