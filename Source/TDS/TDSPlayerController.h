// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TDSPlayerController.generated.h"

class UTDS_InventoryWidget;
class UTDS_InventoryWeaponStatWidget;
class UTDSInventoryComponent;
class UTDS_CharacterHealthComponent;

UCLASS()
class TDS_API ATDSPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:

	void CreateInventoryStatsWidget(FName WeaponName);

	void DestroyInventoryWidget();

	void SetVisibilityVisible();
	void SetVisibilityCollapsed();

	void DestroyRowWidgetForName(FName WeaponName);
	bool AddToInventory(FName NewWeaponName);

	void ChangeWeaponSlot(FName WeaponName);
	

protected:
	virtual void OnPossess(APawn* aPawn) override;

private:

	UTDSInventoryComponent* GetInventoryComponent();

	UTDS_InventoryWidget* InventoryWidgetPointer;

	UTDS_InventoryWeaponStatWidget* StatWidgetPointer;


};
