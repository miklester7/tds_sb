// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/TDSBaseWeapon.h"
#include "TDSPlayerController.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"
#include "Sound/SoundCue.h"
#include "Math/UnrealMathUtility.h"
#include "Math/Rotator.h"
#include "Components/ArrowComponent.h"
#include "Components/SceneComponent.h"
#include "Engine/StaticMeshActor.h"
#include "Weapon/Projectile/TDS_ProjectileBase.h"
#include "Animation/AnimNotify/TDSRealoadAnimNotify.h"
#include "PlayerCharacter.h"
#include "Components/TDSInventoryComponent.h"
#include "UI/TDS_MainHUDWidget.h"
#include "Weapon/DamageType/TDS_CritDamageType.h"
#include "MainCharacter.h"
#include "TDSUtils.h"

DEFINE_LOG_CATEGORY_STATIC(BaseWeaponLog, All, All);

int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(TEXT("TDS.DebugExplode"), DebugExplodeShow, TEXT("Draw debug for Explode"), ECVF_Cheat);

ATDSBaseWeapon::ATDSBaseWeapon()
{
	PrimaryActorTick.bCanEverTick = false;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	SetRootComponent(SceneComponent);

	SkeletalMeshComponent = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("SkeletalMesh"));
	SkeletalMeshComponent->SetupAttachment(SceneComponent);
	SkeletalMeshComponent->SetGenerateOverlapEvents(false);
	SkeletalMeshComponent->SetCollisionProfileName(TEXT("NoCollison"));
	SkeletalMeshComponent->SetRenderCustomDepth(true);

	StaticMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	StaticMeshComponent->SetupAttachment(SceneComponent);
	StaticMeshComponent->SetGenerateOverlapEvents(false);
	StaticMeshComponent->SetCollisionProfileName(TEXT("NoCollison"));

	DebugArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("DebugArrow"));
	DebugArrow->SetupAttachment(SceneComponent);
}

void ATDSBaseWeapon::BeginPlay()
{
	Super::BeginPlay();

	if (!bIsNeedArrow)
	{
		DebugArrow->DestroyComponent();
	}
}

void ATDSBaseWeapon::WeaponInit()
{
	if (SkeletalMeshComponent && !SkeletalMeshComponent->SkeletalMesh)
	{
		SkeletalMeshComponent->DestroyComponent(true);
	}
	if (StaticMeshComponent && !StaticMeshComponent->GetStaticMesh())
	{
		StaticMeshComponent->DestroyComponent();
	}

	//DebugArrow->DestroyComponent();

	if (WeaponSettings.bIsWeaponHaveClip && WeaponSettings.ClipDrop.DropMesh)
	{
		UE_LOG(BaseWeaponLog, Warning, TEXT("Clip init"));
		InitClip(WeaponSettings.ClipDrop.DropMeshTransform, WeaponSettings.ClipDrop.DropMesh);
	}

	auto EquipFinishedNotify = FindNotifyByClass<UTDSRealoadAnimNotify>(WeaponSettings.ReloadAnimMontage);
	if (EquipFinishedNotify)
	{
		UE_LOG(BaseWeaponLog, Warning, (TEXT("Notify is found")));
		EquipFinishedNotify->OnNotified.AddUObject(this, &ATDSBaseWeapon::OnReloadFinishd);
	}

	GetAmmoFromInventory();
}

void ATDSBaseWeapon::InitAmmo(bool ForOneBullet)
{
	const auto CurrentCharacter = Cast<APlayerCharacter>(GetInstigator());
	const auto InventoryComponent = TDSUtils::GetTDSComponentByClass<UTDSInventoryComponent>(CurrentCharacter);
	if (!CurrentCharacter || !InventoryComponent) return;

	if (!ForOneBullet)
	{
		int32 NeedAmmo = WeaponSettings.DefaultBullets - AmmoData.BulletsInClip;

		if (InventoryComponent->GetWeaponAmmoForType(WeaponSettings.WeaponType, AmmoData.Bullets, NeedAmmo))
		{
			AmmoData.BulletsInClip += NeedAmmo;
		}
	}
	else
	{
		int32 NeedAmmo = 1;

		if (InventoryComponent->GetWeaponAmmoForType(WeaponSettings.WeaponType, AmmoData.Bullets, NeedAmmo))
		{
			AmmoData.BulletsInClip += NeedAmmo;
		}
	}
	
}

void ATDSBaseWeapon::StartFire()
{
	if (WeaponSettings.Automatic)
	{
		GetWorldTimerManager().SetTimer(ShotTimerHandle, this, &ATDSBaseWeapon::AnyShot, WeaponSettings.RateOfFire, true, 0);
	}
	else
	{
		AnyShot();
	}
}

void ATDSBaseWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(ShotTimerHandle);
}

void ATDSBaseWeapon::MakeShot()
{
	FVector TraceStart, TraceEnd;
	if (!GetShootingPointCoordinates(TraceStart, TraceEnd)) return;

	FHitResult HitResult;
	MakeHit(HitResult, TraceStart, TraceEnd);

	UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSettings.WeaponShotCue, SkeletalMeshComponent->GetSocketLocation(WeaponSettings.MuzzleSocketName));
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSettings.MuzzleFX, SkeletalMeshComponent->GetSocketLocation(WeaponSettings.MuzzleSocketName),
	SkeletalMeshComponent->GetSocketRotation(WeaponSettings.MuzzleSocketName), FVector(0.4, 0.4, 0.4));

	if (DebugExplodeShow)
	{
		DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Red, false, 2,0,2);
	}
	
	if (HitResult.bBlockingHit)
	{
		OnAnyHit(HitResult);
		if (DebugExplodeShow)
		{
			DrawDebugSphere(GetWorld(), HitResult.Location, 8, 8, FColor::Green, false, 3, 0, 2);
		}
		MakeDamage(HitResult);
	}
	DecreaseAmmo();
}

void ATDSBaseWeapon::MakeDamage(const FHitResult HitResult)
{
	const auto CurrentController = GetOwner<ATDSPlayerController>();
	const auto PawnInstigator = GetInstigator();
	if (!CurrentController || !PawnInstigator) return;

	float Damage = 0;
	bool bIsCrit = CalculateDamage(Damage);

	if (bIsCrit)
	{
		UGameplayStatics::ApplyPointDamage(
			HitResult.GetActor(),
			Damage,
			HitResult.ImpactPoint,
			HitResult,
			CurrentController,
			PawnInstigator,
			DamageType);
	}
	else
	{
		UGameplayStatics::ApplyPointDamage(
			HitResult.GetActor(),
			Damage,
			HitResult.ImpactPoint,
			HitResult,
			CurrentController,
			PawnInstigator,
			nullptr);
	}
}

void ATDSBaseWeapon::MakeShotgunShot()
{
	for (int32 i = 0; i < WeaponSettings.FractionsNumber; i++)
	{
		FVector TraceStart, TraceEnd;
		if (!GetShootingPointCoordinates(TraceStart, TraceEnd)) return;

		FHitResult HitResult;
		MakeHit(HitResult, TraceStart, TraceEnd);

		if (DebugExplodeShow)
		{
			DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Red, false, 2, 0, 2);
		}

		if (HitResult.bBlockingHit)
		{
			OnAnyHit(HitResult);
			if (DebugExplodeShow)
			{
				DrawDebugSphere(GetWorld(), HitResult.Location, 8, 8, FColor::Green, false, 3, 0, 2);
			}

			MakeDamage(HitResult);
		}
	}

	UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSettings.WeaponShotCue, SkeletalMeshComponent->GetSocketLocation(WeaponSettings.MuzzleSocketName));
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSettings.MuzzleFX, SkeletalMeshComponent->GetSocketLocation(WeaponSettings.MuzzleSocketName),
		SkeletalMeshComponent->GetSocketRotation(WeaponSettings.MuzzleSocketName), FVector(0.4, 0.4, 0.4));

	DecreaseAmmo();
}

bool ATDSBaseWeapon::IsAmmoEmpty() const
{
	return AmmoData.Bullets < 1 && !WeaponSettings.Infinite && AmmoData.BulletsInClip < 1;
}

void ATDSBaseWeapon::Reload()
{
	if (WeaponSettings.WeaponType == EWeaponType::Launcher_Type && AmmoData.BulletsInClip != 0) return;
	if (!CanReload()) return;

	const auto CurrentCharacter = Cast<APlayerCharacter>(GetInstigator());
	if (CurrentCharacter)
	{
		CurrentCharacter->PlayAnimMontage(WeaponSettings.ReloadAnimMontage);
		ReloadInProgress = true;
		DropClip();
		UE_LOG(BaseWeaponLog, Display, TEXT("Reload"));
	}
	//��������� ��������!
	if (WeaponSettings.WeaponType == EWeaponType::Shotgun_Type)
	{
		InitAmmo(true);
	}
	//
	else
	{
		if (!WeaponSettings.Infinite)
		{
			InitAmmo();
		}
		else
		{
			AmmoData.BulletsInClip = WeaponSettings.DefaultBullets;
		}	
	}

	const auto Widget = CurrentCharacter->HUDWidget;
	if (Widget)
	{
		Widget->SetAmmoClip(AmmoData.BulletsInClip);
		Widget->SetAllAmmo(AmmoData.Bullets);
	}
}

void ATDSBaseWeapon::DecreaseAmmo()
{
	AmmoData.BulletsInClip--;

	if (AmmoData.BulletsInClip == 0 && !IsAmmoEmpty())
	{
		Reload();
	}
}

bool ATDSBaseWeapon::GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation) const
{
	const auto CuurentController = Cast<ATDSPlayerController>(GetOwner());

	CuurentController->GetPlayerViewPoint(ViewLocation, ViewRotation);

	return true;
}

void ATDSBaseWeapon::MakeHit(FHitResult& HitResult, const FVector& TraceStart, const FVector& TraceEnd)
{
	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(GetOwner());
	CollisionParams.bReturnPhysicalMaterial = true;

	GetWorld()->LineTraceSingleByChannel(HitResult, TraceStart, TraceEnd, ECollisionChannel::ECC_Visibility, CollisionParams);
}

// + ��������,����� �� �������� �� �����
bool ATDSBaseWeapon::GetShootingPointCoordinates(FVector& TraceStart, FVector& TraceEnd)
{
	FVector ViewLocation;
	FRotator ViewRotation;

	if (!GetPlayerViewPoint(ViewLocation, ViewRotation)) return false;

	const auto HalfRad = FMath::DegreesToRadians(WeaponSettings.Dispersion);
	const FVector ShootDirection = FMath::VRandCone(ViewRotation.Vector(), HalfRad);
	TraceEnd = ViewLocation + ShootDirection * WeaponSettings.FireDistance;

	const FVector MuzzleLocation = GetMuzzleWorldLocation();
	const FVector VectorCameraToMuzzle = MuzzleLocation - ViewLocation;

	const FVector ProjectedVector = VectorCameraToMuzzle.ProjectOnToNormal(ViewRotation.Vector());

	TraceStart = ViewLocation + ProjectedVector;

	if (DebugExplodeShow)
	{
		DrawDebugLine(GetWorld(), MuzzleLocation, TraceStart, FColor::Green, false, 3.0f, 0, 2.0f);
	}
	

	return true;
}

void ATDSBaseWeapon::InitClip(FTransform& SpawnLocation, UStaticMesh* ClipMesh)
{
	if (!WeaponSettings.ClipDrop.DropMesh) return;

	FActorSpawnParameters SpawnParametrs;
	SpawnParametrs.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParametrs.Owner = this;
	
	AStaticMeshActor* SpawnedActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(),WeaponSettings.ClipDrop.DropMeshTransform,SpawnParametrs);

	if (SpawnedActor && SpawnedActor->GetStaticMeshComponent())
	{
		SpawnedActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		SpawnedActor->SetActorTickEnabled(false);
		SpawnedActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
		SpawnedActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
		SpawnedActor->GetStaticMeshComponent()->SetStaticMesh(WeaponSettings.ClipDrop.DropMesh);

		FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);

		SpawnedActor->AttachToActor(this, Rule, "ClipSocket");

		const auto ClipTransform =SkeletalMeshComponent->GetSocketTransform("ClipSocket");
		SpawnedActor->SetActorTransform(ClipTransform);

		CurrentClip = SpawnedActor;
	}
}

void ATDSBaseWeapon::DropClip()
{
	if (!CurrentClip) return;
	
	CurrentClip->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);

	CurrentClip->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
	CurrentClip->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	CurrentClip->GetStaticMeshComponent()->SetSimulatePhysics(true);
	CurrentClip->SetLifeSpan(WeaponSettings.ClipDrop.LifeSpane);
	CurrentClip = nullptr;
}

void ATDSBaseWeapon::DropShell()
{
	if (!WeaponSettings.ShellDrop.DropMesh) return;

	FTransform Transform,DropMeshOffset = WeaponSettings.ShellDrop.DropMeshTransform;

	FVector LocalDir = this->GetActorForwardVector() * DropMeshOffset.GetLocation().X +
		this->GetActorRightVector() * DropMeshOffset.GetLocation().Y + this->GetActorUpVector() * DropMeshOffset.GetLocation().Z;

	Transform.SetLocation(GetActorLocation() + LocalDir);
	Transform.SetScale3D(DropMeshOffset.GetScale3D());
	Transform.SetRotation(GetActorRotation().Quaternion() + DropMeshOffset.GetRotation());

	AStaticMeshActor* ShellActor = nullptr;

	FActorSpawnParameters Param;

	Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	Param.Owner = this;

	ShellActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Param);

	if (ShellActor && ShellActor->GetStaticMeshComponent())
	{
		ShellActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
		ShellActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		ShellActor->SetActorTickEnabled(false);

		ShellActor->SetLifeSpan(WeaponSettings.ShellDrop.LifeSpane);
		ShellActor->GetStaticMeshComponent()->SetMobility(EComponentMobility::Movable);
		ShellActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
		ShellActor->GetStaticMeshComponent()->SetStaticMesh(WeaponSettings.ShellDrop.DropMesh);
	}

	if (WeaponSettings.ShellDrop.CustomMass > 0.f)
	{
		ShellActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None,WeaponSettings.ShellDrop.CustomMass);
	}

	if (!WeaponSettings.ShellDrop.ImpulsVector.IsNearlyZero())
	{
		FVector FinalVector;
		LocalDir = LocalDir + (WeaponSettings.ShellDrop.ImpulsVector * 1000.f);
		if (!FMath::IsNearlyZero(WeaponSettings.ShellDrop.ImpulsDispersion))
		{
			FinalVector = UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, WeaponSettings.ShellDrop.ImpulsDispersion);
		}
		FinalVector.GetSafeNormal(0.0001f);
		ShellActor->GetStaticMeshComponent()->AddImpulse(FinalVector * WeaponSettings.ShellDrop.ImpulsPower);
	}
}

void ATDSBaseWeapon::AnyShot()
{
	if (IsAmmoEmpty() || ReloadInProgress || !CanFire && !WeaponSettings.Automatic)
	{
		StopFire();
		return;
	}

	if (AmmoData.BulletsInClip == 0)
	{
		Reload();
		StopFire();
		return;
	}

	DropShell();

	switch (WeaponSettings.WeaponType)
	{
		case EWeaponType::Rifle_Type:
			MakeShot();
			break;
		case EWeaponType::Launcher_Type:
			LaunchShot();
			break;
		case EWeaponType::Shotgun_Type:
			MakeShotgunShot();
			break;
		default:
			MakeShot();
			break;
	}
	CanFire = false;

	const APlayerCharacter* CurrentCharacter = GetInstigator<APlayerCharacter>();
	if (CurrentCharacter)
	{
		const auto Widget = CurrentCharacter->HUDWidget;
		if (Widget)
		{
			Widget->SetAmmoClip(AmmoData.BulletsInClip);
		}
		
	}

	if (!WeaponSettings.Automatic)
	{
		GetWorldTimerManager().SetTimer(CanFireTimerHandle, this, &ATDSBaseWeapon::PossibleToShoot, 0.01, false, WeaponSettings.RateOfFire);
	}
}

void ATDSBaseWeapon::PossibleToShoot()
{
	CanFire = true;
	GetWorldTimerManager().ClearTimer(CanFireTimerHandle);
}

void ATDSBaseWeapon::LaunchShot()
{
	FVector TraceStart, TraceEnd;
	if (!GetShootingPointCoordinates(TraceStart, TraceEnd)) return;

	FHitResult HitResult;
	MakeHit(HitResult, TraceStart, TraceEnd);

	
	const FVector EndPoint = HitResult.bBlockingHit ? HitResult.ImpactPoint : TraceEnd;
	const FVector Direction = (EndPoint - GetMuzzleWorldLocation()).GetSafeNormal();

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnParams.Owner = GetOwner();
	SpawnParams.Instigator = GetInstigator();

	const FRotator SpawnRotation = Direction.Rotation();
	const FVector SpawnLocation = GetMuzzleWorldLocation();

	if (!WeaponSettings.ProjectileSettings.ProjectileClass) return;

	ATDS_ProjectileBase* Projectile = Cast<ATDS_ProjectileBase>(GetWorld()->SpawnActor(WeaponSettings.ProjectileSettings.ProjectileClass, &SpawnLocation, &SpawnRotation, SpawnParams));

	UGameplayStatics::PlaySoundAtLocation(GetWorld(), WeaponSettings.WeaponShotCue, SkeletalMeshComponent->GetSocketLocation(WeaponSettings.MuzzleSocketName));
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSettings.MuzzleFX, SkeletalMeshComponent->GetSocketLocation(WeaponSettings.MuzzleSocketName));

	if (Projectile)
	{
		Projectile->SetDirection(Direction);
		Projectile->SetInitSettings(WeaponSettings.ProjectileSettings);
	}

	if (DebugExplodeShow)
	{
		DrawDebugLine(GetWorld(), GetMuzzleWorldLocation(), TraceEnd, FColor::Red, false, 2, 0, 0.5);
		Projectile->bShowDebugSphere = true;
	}

	DecreaseAmmo();
}

FVector ATDSBaseWeapon::GetMuzzleWorldLocation()
{
	return SkeletalMeshComponent->GetSocketLocation(WeaponSettings.MuzzleSocketName);
}

void ATDSBaseWeapon::OnAnyHit(FHitResult& HitResult)
{
	if (WeaponSettings.HitFX.IsEmpty() || WeaponSettings.BulletImpactCue.IsEmpty()) return;

	EPhysicalSurface ObjectSurfaceType = UGameplayStatics::GetSurfaceType(HitResult);

	if (!WeaponSettings.HitFX.Contains(ObjectSurfaceType)) return;

	UParticleSystem* HitFX = WeaponSettings.HitFX[ObjectSurfaceType];

	if (HitFX)
	{
		UE_LOG(BaseWeaponLog, Display, TEXT("Surface found!"));
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), HitFX, HitResult.ImpactPoint, HitResult.ImpactNormal.Rotation(), FVector(0.4, 0.4, 0.4));
	}

	if (!WeaponSettings.BulletImpactCue.Contains(ObjectSurfaceType)) return;
	
	USoundCue* HitCue = WeaponSettings.BulletImpactCue[ObjectSurfaceType];

	if (HitCue)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), HitCue, HitResult.ImpactPoint);
	}
}

void ATDSBaseWeapon::OnReloadFinishd(USkeletalMeshComponent* MeshComp)
{
	ACharacter* Character = Cast<ACharacter>(GetInstigator());
	if (!Character || MeshComp != Character->GetMesh()) return;

	UE_LOG(BaseWeaponLog, Display, TEXT("Reload is progress = false"));
	InitClip(WeaponSettings.ClipDrop.DropMeshTransform, WeaponSettings.ClipDrop.DropMesh);
	ReloadInProgress = false;
}

bool ATDSBaseWeapon::CanReload()
{
	const AMainCharacter* Character = GetInstigator<AMainCharacter>();
	if (!Character)
	{
		UE_LOG(BaseWeaponLog, Error, TEXT("(CanReload) Character = nullptr!"));
		return false;
	}
	UE_LOG(BaseWeaponLog, Error, TEXT("Character is running %i"), Character->IsRunning);
	return !ReloadInProgress && AmmoData.Bullets != 0 && !(AmmoData.BulletsInClip == WeaponSettings.DefaultBullets) && !Character->IsRunning;
}

void ATDSBaseWeapon::DestroyClip()
{
	if (CurrentClip)
	{
		CurrentClip->Destroy();
		CurrentClip = nullptr;
	}
}

void ATDSBaseWeapon::GetAmmoFromInventory()
{
	const auto CurrentCharacter = Cast<APlayerCharacter>(GetInstigator());
	const auto InventoryComponent = TDSUtils::GetTDSComponentByClass<UTDSInventoryComponent>(CurrentCharacter);
	if (!CurrentCharacter || !InventoryComponent) return;

	InventoryComponent->FindAmmunition(AmmoData.Bullets, WeaponSettings.WeaponType,false);
}

bool ATDSBaseWeapon::CalculateDamage(float& Damage)
{
	float Chance = FMath::FRandRange(0.f, 1.0f);

	if (Chance <= WeaponSettings.CritChance)
	{
		Damage = WeaponSettings.WeaponDamage * WeaponSettings.CritDamage;
		return true;
	}

	Damage = WeaponSettings.WeaponDamage;

	return false;
}
