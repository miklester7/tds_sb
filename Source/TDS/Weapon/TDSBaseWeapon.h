// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Types.h"
#include "TDSBaseWeapon.generated.h"

class USceneComponent;
class USkeletalMeshComponent;
class UStaticMeshComponent;
class UArrowComponent;
class AStaticMeshActor;
class UTDS_CritDamageType;

USTRUCT(BlueprintType)
struct FWeaponAmmoData
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Ammo")
	int32 BulletsInClip = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Ammo")
	int32 Bullets = 0;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Ammo")
	bool WeaponCharged = true;
};

UCLASS()
class TDS_API ATDSBaseWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	ATDSBaseWeapon();

	UPROPERTY(VisibleAnywhere,BlueprintReadOnly,Category = Components)
	USceneComponent* SceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	USkeletalMeshComponent* SkeletalMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UStaticMeshComponent* StaticMeshComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UArrowComponent* DebugArrow;
	 
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "FireLogic")
	FWeaponInfo WeaponSettings;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	FWeaponAmmoData AmmoData;

	AStaticMeshActor* CurrentClip = nullptr;
	void DestroyClip();

	bool ReloadInProgress = false;
	void OnReloadFinishd(USkeletalMeshComponent* MeshComp);
	bool CanReload();

	void WeaponInit();

	void StartFire();

	void StopFire();

	void DropClip();

	void DropShell();

	void Reload();

	void InitAmmo(bool ForOneBullet = false);

	void GetAmmoFromInventory();
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "Debug")
	bool bIsNeedArrow = false;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<UTDS_CritDamageType> DamageType; //Test
protected:
	virtual void BeginPlay() override;

	FVector GetMuzzleWorldLocation();

	void AnyShot();

	void MakeShot();
	void MakeShotgunShot();
	void OnAnyHit(FHitResult& HitResult);

	void MakeDamage(const FHitResult HitResult);

	void LaunchShot();

	void DecreaseAmmo();

	bool IsAmmoEmpty() const;

	FTimerHandle ShotTimerHandle;

	FTimerHandle CanFireTimerHandle;
	bool CanFire = true;
	void PossibleToShoot();

	void MakeHit(FHitResult& HitResult, const FVector& TraceStart, const FVector& TraceEnd);

	bool GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation) const;

	bool GetShootingPointCoordinates(FVector& Start, FVector& End);

	void InitClip(FTransform& SpawnLocation, UStaticMesh* ClipMesh);

	bool CalculateDamage(float& Damage);

	//��������� � Utils? �
	template<typename T>
	T* FindNotifyByClass(UAnimSequenceBase* Animation)
	{
		if (!Animation) return nullptr;

		const auto NotifyEvents = Animation->Notifies;
		for (auto NotifyEvent : NotifyEvents)
		{
			auto AnimNotify = Cast<T>(NotifyEvent.Notify);
			if (AnimNotify)
			{
				return AnimNotify;
			}
		}

		return nullptr;
	}
};
