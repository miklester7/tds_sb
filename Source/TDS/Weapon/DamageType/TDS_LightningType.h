// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/DamageType.h"
#include "TDS_LightningType.generated.h"

UCLASS()
class TDS_API UTDS_LightningType : public UDamageType
{
	GENERATED_BODY()
	
};
