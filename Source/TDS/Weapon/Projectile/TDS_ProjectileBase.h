// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Types.h"
#include "TDS_ProjectileBase.generated.h"

class USphereComponent;
class UProjectileMovementComponent;
class UParticleSystemComponent;

UCLASS()
class TDS_API ATDS_ProjectileBase : public APawn
{
	GENERATED_BODY()

public:

	ATDS_ProjectileBase();

	UPROPERTY(EditDefaultsOnly)
		USphereComponent* CollisionComponent;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite,Category = "ProjectileMovement")
		UProjectileMovementComponent* MovementComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
		UParticleSystemComponent* ProjectileFXComponent = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
		UAudioComponent* ProjectileAudioComponent = nullptr;

	void SetDirection(const FVector Direction) { ProjectileDirection = Direction; }

	void SetInitSettings(FProjectileSettings& Settings);

	bool bShowDebugSphere = false;

protected:

	virtual void BeginPlay() override;

	UFUNCTION()
	void OnProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UPROPERTY(EditDefaultsOnly, Category = "ProjectileMovement")
	FProjectileSettings InitSettings;

	FVector ProjectileDirection;

	FTimerHandle AutoExplosionTimerHandle;
	void ProjectileExplosion();

};
