// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon/Projectile/TDS_ProjectileBase.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/SphereComponent.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/AudioComponent.h"
#include "PlayerCharacter.h"

DEFINE_LOG_CATEGORY_STATIC(BaseProjectileLog, All, All);

ATDS_ProjectileBase::ATDS_ProjectileBase()
{
	PrimaryActorTick.bCanEverTick = false;

	CollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("SphereCollision"));
	SetRootComponent(CollisionComponent);

	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("MovementComponent"));

	CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
	CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	CollisionComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Ignore);
	
	ProjectileFXComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("FXComponent"));
	ProjectileFXComponent->SetupAttachment(CollisionComponent);

	ProjectileAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("AudioComponent"));
	ProjectileAudioComponent->SetupAttachment(CollisionComponent);

//	AActor* MyOwner = Cast<AActor>(GetOwner());
	//CollisionComponent->IgnoreActorWhenMoving(MyOwner, true);
	//CollisionComponent->MoveIgnoreActors
}

void ATDS_ProjectileBase::BeginPlay()
{
	Super::BeginPlay();

	CollisionComponent->OnComponentHit.AddDynamic(this, &ATDS_ProjectileBase::OnProjectileHit);
}

void ATDS_ProjectileBase::OnProjectileHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	if (bShowDebugSphere)
	{
		DrawDebugSphere(GetWorld(), Hit.ImpactPoint, InitSettings.DamageInnerRadius, 24, FColor::Red, false, 6.f);
		DrawDebugSphere(GetWorld(), Hit.ImpactPoint, InitSettings.DamageExternalRadius, 24, FColor::Yellow, false, 6.f);
	}

	ProjectileExplosion();
}

void ATDS_ProjectileBase::SetInitSettings(FProjectileSettings& Settings)
{ 
	InitSettings = Settings;

	GetWorldTimerManager().SetTimer(AutoExplosionTimerHandle, this, &ATDS_ProjectileBase::ProjectileExplosion, 1.f, false, InitSettings.ProjectileLifeTime);

	MovementComponent->InitialSpeed = InitSettings.ProjectileInitSpeed;
	MovementComponent->Velocity = ProjectileDirection * MovementComponent->InitialSpeed;
	MovementComponent->bRotationFollowsVelocity = false;

	if (InitSettings.bIsRocket)
	{
		MovementComponent->ProjectileGravityScale = 0;
	}
	else
	{
		MovementComponent->ProjectileGravityScale = 1;
		CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		CollisionComponent->SetSimulatePhysics(true);
	}
}

void ATDS_ProjectileBase::ProjectileExplosion()
{
	MovementComponent->StopMovementImmediately();

	UGameplayStatics::PlaySoundAtLocation(GetWorld(), InitSettings.ExplosionCue, GetActorLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), InitSettings.ExplosionFX, GetActorLocation());

	const auto CurrentOwner = GetOwner<APlayerController>();
	if (!CurrentOwner)
	{
		UE_LOG(BaseProjectileLog, Error, TEXT("Owner is nullptr"));
		Destroy();
		return;
	}

	TArray<AActor*> IgnorActors;

	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		InitSettings.MaxDamage,
		InitSettings.MinDamage,
		GetActorLocation(),
		InitSettings.DamageInnerRadius,
		InitSettings.DamageExternalRadius,
		5,
		nullptr, IgnorActors, GetInstigator(), CurrentOwner
	);

	Destroy();
}

