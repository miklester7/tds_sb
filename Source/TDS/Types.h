#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "Sound/SoundCue.h"
#include "Types.generated.h"

UENUM(BlueprintType)
enum class ELocomotionType : uint8
{
	Aim_Type UMETA(DisplayName = "Aim"),
	Walk_Type UMETA(DisplayName = "Walk"),
};

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	Rifle_Type UMETA(DisplayName = "Rifle"),
	Sniper_Type UMETA(DisplayName = "Sniper"),
	Shotgun_Type UMETA(DisplayName = "Shotgun"),
	Launcher_Type UMETA(DisplayName = "Launcher"),
};

UENUM(BlueprintType)
enum class EPickupType : uint8
{
	Health_Type UMETA(DisplayName = "HealthRegeneration"),
};

USTRUCT(BlueprintType)
struct FAmmoData     
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EWeaponType AmmoType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		int32 AmmoCount = 0;

		int32 AmmoInClip = 0;
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float AimSpeed = 200.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float WalkSpeed = 600.0f;
};

USTRUCT(BlueprintType)
struct FDropMeshInfo
{
	GENERATED_BODY()

		UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DropMesh")
		UStaticMesh* DropMesh = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DropMesh")
		FTransform DropMeshTransform = FTransform();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DropMesh")
		FVector ImpulsVector = FVector(0);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DropMesh")
		float ImpulsPower = 0.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DropMesh")
		float ImpulsDispersion = 0.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DropMesh")
		float CustomMass = 0.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DropMesh")
		float LifeSpane = 0.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "DropMesh")
		USoundCue* BulletImpactCue = nullptr;
};

USTRUCT(BlueprintType)
struct FProjectileSettings
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Mesh")
		TSubclassOf<class ATDS_ProjectileBase> ProjectileClass = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
		float ProjectileInitSpeed = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
		float ProjectileLifeTime = 10.0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
		float DamageInnerRadius = 100.0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
		float DamageExternalRadius = 300.0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
		float MinDamage = 10.0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
		float MaxDamage = 100.0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
		bool bIsRocket;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
		bool bDoFullRadialDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sounds")
		USoundCue* ExplosionCue = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FX")
		UParticleSystem* ExplosionFX = nullptr;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		bool bIsWeaponHaveClip;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		int32 WeaponDamage = 3;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		float CritChance = 0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		float CritDamage = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		float Dispersion = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		float FireDistance = 1000.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon", meta = (ClampMin = "1"))
		int32 DefaultBullets = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		int32 FractionsNumber = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
		float RateOfFire = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
		bool Infinite = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
		bool Automatic = false;

	UPROPERTY(EditDefaultsOnly,BlueprintReadWrite, Category = "Mesh")
		TSubclassOf<class ATDSBaseWeapon> WeaponClass = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Projectile")
		FProjectileSettings ProjectileSettings;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
		FName MuzzleSocketName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sounds")
		USoundCue* WeaponShotCue = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sounds")
		TMap<TEnumAsByte<EPhysicalSurface>, USoundCue*> BulletImpactCue;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FX")
		UParticleSystem* MuzzleFX = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FX")
		TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
		EWeaponType WeaponType;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animation")
		UAnimMontage* ReloadAnimMontage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animation")
		FDropMeshInfo ShellDrop;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Animation")
		FDropMeshInfo ClipDrop;
};

USTRUCT(BlueprintType)
struct FDecalsStruct
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Decals")
	UMaterialInterface* DecalMaterial;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Decals")
	FVector DecalSize;
};

USTRUCT(BlueprintType)
struct FDropItemsInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ItemType")
		bool bIsAmmo = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ItemType")
		EWeaponType Type;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ItemType", meta = (EditCondition = "bIsAmmo"))
		float Scale = 1;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "ItemType", meta = (EditCondition = "!bIsAmmo"))
		int32 AmmoInClip = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "PhysicalParameters")
		float CollisionRadius = 32.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AmmoPickup")
		UStaticMesh* ItemMesh = nullptr;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AmmoPickup")
		UTexture2D* InventoryImage = nullptr;

};

UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

};