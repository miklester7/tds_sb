#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"

class TDSUtils
{
public:

	template <typename T>
	static T* GetTDSComponentByClass(AActor* PlayerPawn)
	{
		if (!PlayerPawn) return nullptr;

		const auto Component = PlayerPawn->GetComponentByClass(T::StaticClass());

		return Cast<T>(Component);
	}
};
