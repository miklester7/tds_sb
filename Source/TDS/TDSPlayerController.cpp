// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSPlayerController.h"
#include "UI/TDS_InventoryWeaponStatWidget.h"
#include "Components/Border.h"
#include "TDSGameInstance.h"
#include "Components/TDSInventoryComponent.h"
#include "PlayerCharacter.h"
#include "TDSUtils.h"
#include "Components/TDS_CharacterHealthComponent.h"

void ATDSPlayerController::OnPossess(APawn* aPawn)
{
	Super::OnPossess(aPawn);

	SetInputMode(FInputModeGameOnly());

	const auto inventoryComponent = GetInventoryComponent();
	if (!inventoryComponent) return;

	inventoryComponent->CleaningLines();

	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
	if (!myGI) return;

	const auto InventoryWidget = CreateWidget<UTDS_InventoryWidget>(GetWorld(), myGI->InventoryWidget);
	if (!InventoryWidget) return;
	InventoryWidgetPointer = InventoryWidget;

	InventoryWidget->AddToViewport();
	InventoryWidget->WidgetBorder->ClearChildren();

	InventoryWidgetPointer->SetVisibility(ESlateVisibility::Hidden);
}

void ATDSPlayerController::CreateInventoryStatsWidget(FName WeaponName)
{
	UTDSGameInstance* Instance = GetWorld()->GetGameInstance<UTDSGameInstance>();
	if (!Instance) return;

	UTDS_InventoryWeaponStatWidget* StatWidget = CreateWidget<UTDS_InventoryWeaponStatWidget>(this, Instance->WeaponStatsWidget);
	if (!StatWidget) return;

	StatWidgetPointer = StatWidget;

	FDropItemsInfo ItemInfo;
	FWeaponInfo WeaponInfo;

	if (Instance->GetItemsInfoByName(WeaponName, ItemInfo) && Instance->GetWeaponInfoByName(WeaponName, WeaponInfo))
	{
		const auto Image = ItemInfo.InventoryImage;

		StatWidget->SetWidgetStats(WeaponInfo, WeaponName);
	}

	UBorder* Border = InventoryWidgetPointer->WidgetBorder;
	Border->AddChild(StatWidget);
}

void ATDSPlayerController::DestroyInventoryWidget()
{
	if (!StatWidgetPointer) return;

	StatWidgetPointer->RemoveFromParent();
	StatWidgetPointer = nullptr;
}

void ATDSPlayerController::SetVisibilityVisible()
{
	InventoryWidgetPointer->SetVisibility(ESlateVisibility::Visible);
	SetInputMode(FInputModeGameAndUI());
	bShowMouseCursor = true;
	ClientIgnoreLookInput(true);
}

void ATDSPlayerController::SetVisibilityCollapsed()
{
	InventoryWidgetPointer->SetVisibility(ESlateVisibility::Collapsed);
	SetInputMode(FInputModeGameOnly());
	bShowMouseCursor = false;
	ClientIgnoreLookInput(false);
}

UTDSInventoryComponent* ATDSPlayerController::GetInventoryComponent()
{
	const auto PlayerPawn = GetPawn();
	if (!PlayerPawn) return nullptr;

	const auto inventoryComponent = TDSUtils::GetTDSComponentByClass<UTDSInventoryComponent>(PlayerPawn);
	if (!inventoryComponent) return nullptr;

	return inventoryComponent;
}

void ATDSPlayerController::DestroyRowWidgetForName(FName WeaponName)
{
	UTDSInventoryComponent* InventoryComponent = GetInventoryComponent();
	if (!InventoryComponent) return;

	int32 Index = InventoryComponent->DropWeaponFromInventory(WeaponName);
	if (Index >= 0)
	{
		InventoryWidgetPointer->DestroyRowWodgetForIndex(Index);
	}
}

bool ATDSPlayerController::AddToInventory(FName NewWeaponName)
{
	UTDSInventoryComponent* InventoryComponent = GetInventoryComponent();
	if (!InventoryComponent) return false;

	if (!InventoryComponent->TryGetNewWeapon(NewWeaponName)) return false;

	InventoryWidgetPointer->AddRowWidget(NewWeaponName);

	return true;
}

void ATDSPlayerController::ChangeWeaponSlot(FName WeaponName)
{
	UTDSInventoryComponent* InventoryComponent = GetInventoryComponent();
	if (!InventoryComponent) return;

	int32 Index = InventoryComponent->GetWeaponIndexByName(WeaponName);
	if (Index >= 0)
	{
		const auto PlayerPawn = GetPawn<APlayerCharacter>();
		if (!PlayerPawn) return;

		PlayerPawn->SetWeaponFromSlot(Index);
	}
}
