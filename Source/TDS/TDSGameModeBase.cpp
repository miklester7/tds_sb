// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameModeBase.h"
#include "PlayerCharacter.h"
#include "TDSPlayerController.h"
#include "TemporaryClasses/TDSTemporaryHUDClass.h"

ATDSGameModeBase::ATDSGameModeBase()
{
	DefaultPawnClass = APlayerCharacter::StaticClass();
	PlayerControllerClass = ATDSPlayerController::StaticClass();
	HUDClass = ATDSTemporaryHUDClass::StaticClass();

}