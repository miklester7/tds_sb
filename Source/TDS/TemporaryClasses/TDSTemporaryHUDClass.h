// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "TDSTemporaryHUDClass.generated.h"

/**
 * 
 */
UCLASS()
class TDS_API ATDSTemporaryHUDClass : public AHUD
{
	GENERATED_BODY()
public:
		virtual void DrawHUD() override;

		void DrawCrossHair();
};
