// Fill out your copyright notice in the Description page of Project Settings.

#include "MainCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Components/TDS_CharacterMovementComponent.h"
#include "Components/TDS_CharacterHealthComponent.h"
#include "UI/TDS_GDamageActor.h"
#include "TDSPlayerController.h"

AMainCharacter::AMainCharacter(const FObjectInitializer& ObjInit)
	:Super(ObjInit.SetDefaultSubobjectClass<UTDS_CharacterMovementComponent>(ACharacter::CharacterMovementComponentName))
{
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = false;
	GetCharacterMovement()->bSnapToPlaneAtStart = false;

	HealthComponent = CreateDefaultSubobject<UTDS_CharacterHealthComponent>(TEXT("HelthComponet"));

	PrimaryActorTick.bCanEverTick = false;
	PrimaryActorTick.bStartWithTickEnabled = false;
}

void AMainCharacter::BeginPlay()
{
	Super::BeginPlay();

	HealthComponent->OnDeath.AddDynamic(this, &AMainCharacter::OnDeath);
	HealthComponent->TakeDamage.AddUObject(this, &AMainCharacter::SpawnDamageWidget);
}

void AMainCharacter::OnStartRunning()
{
	if (LocomotionType == ELocomotionType::Aim_Type) return;

	const auto Component = GetComponentByClass(UTDS_CharacterMovementComponent::StaticClass());
	const auto MovementComponent = Cast<UTDS_CharacterMovementComponent>(Component);
	MovementComponent->StartRun();
}

void AMainCharacter::OnStopRunnung()
{
	const auto Component = GetComponentByClass(UTDS_CharacterMovementComponent::StaticClass());
	const auto MovementComponent = Cast<UTDS_CharacterMovementComponent>(Component);
	MovementComponent->StopRun();
}


void AMainCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;

	switch (LocomotionType)
	{
	case ELocomotionType::Aim_Type:
		ResSpeed = CharacterSpeed.AimSpeed;
		IsAiming = true;
		OnStopRunnung();
		break;
	case ELocomotionType::Walk_Type:
		ResSpeed = CharacterSpeed.WalkSpeed;
		IsAiming = false;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void AMainCharacter::ChangeMovementType(ELocomotionType NewType)
{
	LocomotionType = NewType;
	CharacterUpdate();
}

void AMainCharacter::SpawnDamageWidget(const float Damage, FVector& Location, const FLinearColor& Color)
{
	if (Location.IsZero())
	{
		Location = GetActorLocation() + DamageWidgetLocation;
	}

	Location = Location + FMath::FRandRange(-10.f, 10.f);

	ATDS_GDamageActor* Widget = GetWorld()->SpawnActor<ATDS_GDamageActor>(DamageActor, Location, FRotator::ZeroRotator, FActorSpawnParameters());
	if (!Widget) return;

	Widget->SetWidgetInfo(Damage, Color);
}

void AMainCharacter::OnDeath()
{
	GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
	GetMesh()->SetSimulatePhysics(true);

	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	GetCharacterMovement()->DisableMovement();

	SetLifeSpan(5.f);

	const auto CurrentController = GetController<ATDSPlayerController>();

	if (!CurrentController) return;
	CurrentController->UnPossess();
}
