// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"
#include "MainCharacter.h"
#include "Components/TDS_CharacterHealthComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/TDS_CharacterMovementComponent.h"
#include "Components/InputComponent.h"
#include "Misc/App.h"
#include "TimerManager.h"
#include "Math/UnrealMathUtility.h"
#include "GameFramework/PlayerController.h"
#include "InteractiongObjects/TDSDoors.h"
#include "Weapon/TDSBaseWeapon.h"
#include "TDSGameInstance.h"
#include "Components/TDSInventoryComponent.h"
#include "UI/TDS_InventoryWidget.h"
#include "Components/ArrowComponent.h"
#include "TDSPlayerController.h"
#include "UI/TDS_MainHUDWidget.h"
#include "UI/TDS_StatsWidget.h"
#include "Components/BackgroundBlur.h"
#include "InteractiongObjects/TDSMainInteractionObject.h"
#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"


DEFINE_LOG_CATEGORY_STATIC(PlayerCharacterLog, All, All);

APlayerCharacter::APlayerCharacter(const FObjectInitializer& ObjInit) : AMainCharacter(ObjInit)
{
	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(GetRootComponent());
	CameraBoom->SetUsingAbsoluteRotation(false); // Don't want arm to rotate when character does
	CameraBoom->bUsePawnControlRotation = true;
	CameraBoom->TargetArmLength = 200.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	InventoryComponent = CreateDefaultSubobject<UTDSInventoryComponent>(TEXT("InventoryComponent"));

	// Create a camera...
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(CameraBoom);

	DropWeaponArrow = CreateDefaultSubobject<UArrowComponent>(TEXT("DropWeaponArrow"));
	DropWeaponArrow->SetupAttachment(GetRootComponent());
}

void APlayerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::InputAxisX);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::InputAxisY);
	PlayerInputComponent->BindAxis("LookUp", this, &APlayerCharacter::LookUp);
	PlayerInputComponent->BindAxis("TurnAround", this, &APlayerCharacter::TurnAround);
	PlayerInputComponent->BindAction("Run", IE_Pressed, this, &APlayerCharacter::OnStartRunning);
	PlayerInputComponent->BindAction("Run", IE_Released, this, &APlayerCharacter::OnStopRunnung);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &APlayerCharacter::StartFire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &APlayerCharacter::StopFire);
	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &APlayerCharacter::Reload);
	PlayerInputComponent->BindAction("Inventory", IE_Pressed, this, &APlayerCharacter::GoToInventory);
	PlayerInputComponent->BindAction("Inventory", IE_Released, this, &APlayerCharacter::ClosingInventory);
	PlayerInputComponent->BindAction("SetFirstWeaponSlot", IE_Pressed, this, &APlayerCharacter::SetFirstWeaponClot);
	PlayerInputComponent->BindAction("SetSecondWeaponSlot", IE_Pressed, this, &APlayerCharacter::SetSecondWeaponSlot);
	PlayerInputComponent->BindAction("SetThirdWeaponSlot", IE_Pressed, this, &APlayerCharacter::SetThirdWeaponSlot);
	PlayerInputComponent->BindAction("DropWeapon", IE_Pressed, this, &APlayerCharacter::DropWeapon);
}

void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	ensure(MainHUDWidget);
	ensure(StatsWidgetClass);
	const auto MovementComponent = GetCharacterMovement<UTDS_CharacterMovementComponent>();
	check(MovementComponent);

	HealthComponent->OnHealthChange.AddUObject(this, &APlayerCharacter::OnHealthChange);
	HealthComponent->OnShieldCnahge.AddUObject(this, &APlayerCharacter::OnShieldChange);
	HealthComponent->OnShieldRegeneration.AddUObject(this, &APlayerCharacter::SetShieldType);
	HealthComponent->OnShieldPenetration.AddUObject(this, &APlayerCharacter::SetBodyType);
	MovementComponent->OnStaminaChange.AddUObject(this, &APlayerCharacter::OnStaminaChange);

	if (GetFirstInventorySLot())
	{
		CreateHUDWidget();
		WeaponInit(InitWeaponName);
	}
	else
	{
		UE_LOG(PlayerCharacterLog, Error, TEXT("Inventory is empty!"));
	}
	
	CreateStatsWidget();

	SetPhysMaterial();
}

void APlayerCharacter::InputAxisX(float Value)
{
	if (Value == 0.0f) return;
	if (Value < 0.0f) OnStopRunnung();

	AddMovementInput(GetActorForwardVector(), Value);
}

void APlayerCharacter::InputAxisY(float Value)
{
	if (Value == 0.0f || IsRunning) return;

	AddMovementInput(GetActorRightVector(), Value);
}

void APlayerCharacter::LookUp(float Value)
{
	AddControllerPitchInput(Value);
}

void APlayerCharacter::TurnAround(float Value)
{
	AddControllerYawInput(Value);
}

void APlayerCharacter::OnStartRunning()
{
	Super::OnStartRunning();
}
void APlayerCharacter::OnStopRunnung()
{
	Super::OnStopRunnung();
}

void APlayerCharacter::WeaponInit(FName IdWeapon)
{
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());

	FWeaponInfo WeaponInfo;

	if (myGI->GetWeaponInfoByName(IdWeapon, WeaponInfo))
	{
		if (WeaponInfo.WeaponClass)
		{
			FVector SpawnLocation = FVector(0);
			FRotator SpawnRotation = FRotator(0);

			FActorSpawnParameters SpawnParams;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
			SpawnParams.Owner = GetOwner();
			SpawnParams.Instigator = GetInstigator();

			ATDSBaseWeapon* myWeapon = Cast<ATDSBaseWeapon>(GetWorld()->SpawnActor(WeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
			if (myWeapon)
			{
				FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
				myWeapon->AttachToComponent(GetMesh(), Rule, FName("b_RightWeaponSocket"));

				myWeapon->SetActorRotation(GetMesh()->GetSocketRotation("b_RightWeaponSocket"));

				CurrentWeapon = myWeapon;
				
				myWeapon->WeaponSettings = WeaponInfo;
				myWeapon->WeaponInit();

				int32 AmmoForClip = 0;
				if (InventoryComponent->TryGetLoadedClip(WeaponInfo.WeaponType, AmmoForClip))
				{
					myWeapon->AmmoData.BulletsInClip = AmmoForClip;
				}
				UpdateHUDWidget();
			}
		}
	}
	else
	{
		UE_LOG(PlayerCharacterLog, Error, TEXT("Weapon Class = nullptr"));
	}
}

void APlayerCharacter::StartFire()
{
	if (!CurrentWeapon) return;

	CurrentWeapon->StartFire();
}

void APlayerCharacter::StopFire()
{
	if (!CurrentWeapon) return;

	CurrentWeapon->StopFire();
}

void APlayerCharacter::PlayAnimation(UAnimMontage* AnimMontage)
{
	if (AnimMontage)
	{
		PlayAnimMontage(AnimMontage);
	}
}

void APlayerCharacter::Reload()
{
	CurrentWeapon->Reload();
}

void APlayerCharacter::GoToInventory()
{
	ATDSPlayerController* CurrentController = GetController<ATDSPlayerController>();
	CurrentController->SetVisibilityVisible();
}

void APlayerCharacter::ClosingInventory()
{
	ATDSPlayerController* CurrentController = GetController<ATDSPlayerController>();
	CurrentController->SetVisibilityCollapsed();
}

void APlayerCharacter::CreateHUDWidget()
{
	if (!MainHUDWidget) return;

	const auto CurrentController = GetController<ATDSPlayerController>();
	if (!CurrentController) return;

	HUDWidget = CreateWidget<UTDS_MainHUDWidget>(CurrentController, MainHUDWidget);
	HUDWidget->AddToViewport();
}

bool APlayerCharacter::GetFirstInventorySLot()
{
	//Use only BeginPlay()!
	if (InventoryComponent->GetFirstSlot(InitWeaponName))
	{
		return true;
	}
	
	return false;
}

void APlayerCharacter::SetFirstWeaponClot()
{
	SetWeaponFromSlot(0);
}

void APlayerCharacter::SetSecondWeaponSlot()
{
	SetWeaponFromSlot(1);
}

void APlayerCharacter::SetThirdWeaponSlot()
{
	SetWeaponFromSlot(2);
}

void APlayerCharacter::DropWeapon()
{
	if (!CurrentWeapon) return;
	if (InventoryComponent->InventoryWeaponNames.Num() <= 1) return;

	CurrentWeapon->StopFire();

	const FVector SpawnLocation = CurrentWeapon->GetActorLocation();
	const FRotator SpawnRotation = CurrentWeapon->GetActorRotation();

	FVector DropItemVector;
	DropItemVector = DropWeaponArrow->GetForwardVector();

	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
	
	ATDSMainInteractionObject* DropWeapon = GetWorld()->SpawnActor<ATDSMainInteractionObject>(SpawnLocation, SpawnRotation, SpawnParams);

	if (!DropWeapon) return;

	DropWeapon->ItemInit(InitWeaponName);
	DropWeapon->StaticMeshSlot->AddImpulse(DropWeaponArrow->GetForwardVector() * 1400.f);
	DropWeapon->CurrentItemInfo.AmmoInClip = CurrentWeapon->AmmoData.BulletsInClip;

	const auto CurrentController = GetController<ATDSPlayerController>();
	if (!CurrentController) return;
	CurrentController->DestroyRowWidgetForName(InitWeaponName);

	DestroyCurrentWeapon(true);

	int32 Index = InventoryComponent->GetWeaponIndexByName(InitWeaponName);
	if (Index > 0)
	{
		InitWeaponName = InventoryComponent->InventoryWeaponNames[Index - 1];
		WeaponInit(InitWeaponName);
	}
	else if (GetFirstInventorySLot())
	{
		WeaponInit(InitWeaponName);
	}
}

bool APlayerCharacter::TryGetNewItem(FName ItemName)
{
	const auto CurrentController = GetController<ATDSPlayerController>();
	if (!CurrentController) return false;

	if(!CurrentController->AddToInventory(ItemName)) return false;

	return true;
}

void APlayerCharacter::DestroyCurrentWeapon(bool bIsDrop)
{
	if (CurrentWeapon->WeaponSettings.bIsWeaponHaveClip)
	{
		CurrentWeapon->DestroyClip();
	}

	if (!bIsDrop)
	{
		InventoryComponent->SetAmmoInClipForType(CurrentWeapon->WeaponSettings.WeaponType, CurrentWeapon->AmmoData.BulletsInClip);
	}

	CurrentWeapon->Destroy();
	CurrentWeapon = nullptr;
}

void APlayerCharacter::SetWeaponFromSlot(int32 Slot)
{
	FName WeaponName;
	if (InventoryComponent->TryGetWeaponFromSlot(Slot, WeaponName))
	{
		if(InitWeaponName == WeaponName) return;

		DestroyCurrentWeapon();
		InitWeaponName = WeaponName;
		WeaponInit(WeaponName);
	}
}

void APlayerCharacter::UpdateHUDWidget()
{
	UTDSGameInstance* myGI = Cast<UTDSGameInstance>(GetGameInstance());
	if (!myGI || !HUDWidget) return;

	FDropItemsInfo Info;

	if (myGI->GetItemsInfoByName(InitWeaponName, Info))
	{
		UE_LOG(PlayerCharacterLog, Error, TEXT("Update HUD!"));
		HUDWidget->SetWeaponImage(Info.InventoryImage);
		HUDWidget->SetAllAmmo(CurrentWeapon->AmmoData.Bullets);
		HUDWidget->SetAmmoClip(CurrentWeapon->AmmoData.BulletsInClip);
	}

}

void APlayerCharacter::UpdateHUDAllAmmo(EWeaponType WeaponType)
{
	if (!CurrentWeapon) return;

	if (WeaponType == CurrentWeapon->WeaponSettings.WeaponType)
	{
		int32 AllAmmo;
		if (InventoryComponent->FindAmmunition(AllAmmo, WeaponType, false))
		{
			HUDWidget->SetAllAmmo(AllAmmo);
		}
	}
}

void APlayerCharacter::UpdateAllAmmo()
{
	if (!CurrentWeapon) return;

	CurrentWeapon->GetAmmoFromInventory();
}

void APlayerCharacter::CreateStatsWidget()
{
	if (!StatsWidgetClass) return;

	const auto CurrentController = GetController<ATDSPlayerController>();
	if (!CurrentController) return;

	StatsWidget = CreateWidget<UTDS_StatsWidget>(CurrentController, StatsWidgetClass);
	HUDWidget->StatsBlur->AddChild(StatsWidget);
	StatsWidget->AddToViewport();

	StatsWidget->SetHelth(HealthComponent->GetHealth(), HealthComponent->GetHealthPercent());
	StatsWidget->SetShield(HealthComponent->GetShield(), HealthComponent->GetShieldPersent());

	const float DefaultHealth = HealthComponent->GetDefaultHealth();

	StatsWidget->ChangeRatio(DefaultHealth / (DefaultHealth + HealthComponent->GetDefaultShield()));

	StatsWidget->SetStamina(GetCharacterMovement<UTDS_CharacterMovementComponent>()->GetStaminaPercent());
}


void APlayerCharacter::OnHealthChange(float Health, float HealthPercent,bool CanSpawnDecal)
{
	if(CanSpawnDecal) TrySpawnDecal();

	if (!StatsWidget) return;

	StatsWidget->SetHelth(Health, HealthPercent);
}

void APlayerCharacter::OnShieldChange(float Shield, float ShieldPercent)
{
	if (!StatsWidget) return;

	if (ShieldPercent > 1) ChangeRatioForStatsW();
	else StatsWidget->SetShield(Shield, ShieldPercent);
}

void APlayerCharacter::OnStaminaChange(float StaminaPercent)
{
	if (!StatsWidget) return;

	StatsWidget->SetStamina(StaminaPercent);
}



void APlayerCharacter::SetShieldType()
{
	GetMesh()->SetPhysMaterialOverride(ShieldPhysMaterial);
}

void APlayerCharacter::SetBodyType()
{
	//if (GetMesh()->GetBodyInstance()->GetSimplePhysicalMaterial() == BodyPhysMaterial) return;

	GetMesh()->SetPhysMaterialOverride(BodyPhysMaterial);
}

void APlayerCharacter::SetPhysMaterial()
{
	const float Shield = HealthComponent->GetShield();
	if (Shield != 0)
	{
		GetMesh()->SetPhysMaterialOverride(ShieldPhysMaterial);
	}
	else
	{
		GetMesh()->SetPhysMaterialOverride(BodyPhysMaterial);
	}
}

void APlayerCharacter::TrySpawnDecal()//Main
{
	const float Chance = FMath::FRand();
	if (Chance > 0.6f)
	{
		if (!BloodDecals.IsValidIndex(0)) return;

		const int32 Index = FMath::RandHelper(BloodDecals.Num() - 1);

		const auto Delta = GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
		FVector SpawnLocation = GetActorLocation();
		SpawnLocation.Z = SpawnLocation.Z - Delta;
		SpawnLocation.X = SpawnLocation.X + FMath::FRandRange(-10.f, 10.f);
		SpawnLocation.Y = SpawnLocation.Y + FMath::FRandRange(-10.f, 10.f);

		const auto Decal = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), BloodDecals[Index].DecalMaterial, BloodDecals[Index].DecalSize, SpawnLocation, FRotator(-90.f,0.f,0.f), 3.5f);
	}
}

void APlayerCharacter::RemoveEffectFromArray(UTDS_MasterEffect* Effect)
{
	CurrentEffects.Remove(Effect);
}

void APlayerCharacter::ChangeRatioForStatsW()
{
	if (!StatsWidget) return;

	const float DefaultHealth = HealthComponent->GetDefaultHealth();
	const float CurrentShield = HealthComponent->GetCurrentShield();
	const float Ratio = DefaultHealth / (DefaultHealth + CurrentShield);

	StatsWidget->SetAdditionalShieldAndRation(CurrentShield, Ratio);
	
}