// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "TDS_CharacterMovementComponent.generated.h"

class APlayerCharacter;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnStaminaChange, float);

UCLASS()
class TDS_API UTDS_CharacterMovementComponent : public UCharacterMovementComponent
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement", meta = (ClampMin = "0.1", ClampMax = "1"))
		float RunModifier = 0.5f;

	virtual float GetMaxSpeed() const override;
	virtual void OnRegister() override;

	float GetStaminaPercent() { return CurrentStamina / MaxStamina; }

	void StartRun();
	void StopRun();

	FOnStaminaChange OnStaminaChange;
private:
	float MaxStamina = 180.0f;
	float CurrentStamina = 0.0f;

	void StaminaUpdate();
	void StaminaRegeneration();

	FTimerHandle StaminaTimerHandle;
	FTimerHandle StaminaRegenTimerHandle;

};
