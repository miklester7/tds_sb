// Fill out your copyright notice in the Description page of Project Settings.


#include "Components/TDS_CharacterMovementComponent.h"
#include "MainCharacter.h"
#include "Engine/World.h"
#include "PlayerCharacter.h"
#include "TimerManager.h"

DEFINE_LOG_CATEGORY_STATIC(LogMovementComponent, All, All)

void UTDS_CharacterMovementComponent::OnRegister()
{
	Super::OnRegister();

	CurrentStamina = MaxStamina;
}

float UTDS_CharacterMovementComponent::GetMaxSpeed() const
{
	const float MaxSpeed = Super::GetMaxSpeed();
	const AMainCharacter* Player = Cast<AMainCharacter>(GetPawnOwner());

	return Player && Player->IsRunning && !Player->IsAiming && CurrentStamina != 0 ? MaxSpeed + MaxSpeed * RunModifier : MaxSpeed;
}

void UTDS_CharacterMovementComponent::StaminaUpdate()
{
	CurrentStamina = FMath::Clamp(CurrentStamina - 0.4f, 0.0f, MaxStamina);

	if (CurrentStamina == 0)
	{
		StopRun();
	}

	OnStaminaChange.Broadcast(GetStaminaPercent());
}

void UTDS_CharacterMovementComponent::StartRun()
{
	if (GetStaminaPercent() < 0.15f) return;

	AMainCharacter* Player = Cast<AMainCharacter>(GetPawnOwner());
	if (Player->IsAiming) return;

	Player->IsRunning = true;

	GetWorld()->GetTimerManager().ClearTimer(StaminaRegenTimerHandle);
	GetWorld()->GetTimerManager().SetTimer(StaminaTimerHandle, this, &UTDS_CharacterMovementComponent::StaminaUpdate, 0.01, true);

}

void UTDS_CharacterMovementComponent::StopRun()
{
	GetWorld()->GetTimerManager().ClearTimer(StaminaTimerHandle);
	GetWorld()->GetTimerManager().SetTimer(StaminaRegenTimerHandle, this, &UTDS_CharacterMovementComponent::StaminaRegeneration, 0.02, true);

	AMainCharacter* Player = Cast<AMainCharacter>(GetPawnOwner());
	Player->IsRunning = false;
}

void UTDS_CharacterMovementComponent::StaminaRegeneration()
{
	CurrentStamina = FMath::Clamp(CurrentStamina + 0.2f, 0.0f, MaxStamina);

	OnStaminaChange.Broadcast(GetStaminaPercent());

	if (CurrentStamina == MaxStamina)
	{
		GetWorld()->GetTimerManager().ClearTimer(StaminaRegenTimerHandle);
	}
}

