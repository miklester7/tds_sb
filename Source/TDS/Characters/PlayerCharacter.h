// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MainCharacter.h"
#include "InteractiongObjects/Effects/TDS_MasterEffect.h"
#include "PlayerCharacter.generated.h"

class ATDSBaseWeapon;
class UTDSInventoryComponent;
class UTDS_InventoryWidget;
class UTDS_MainHUDWidget;
class UArrowComponent;
class ATDSMainInteractionObject;
class UTDS_StatsWidget;
class UPhysicalMaterial;
class UTDS_MasterEffect;

UCLASS()
class TDS_API APlayerCharacter : public AMainCharacter
{
	GENERATED_BODY()
	
		APlayerCharacter(const FObjectInitializer& ObjInit);

	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void BeginPlay() override;

	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return CameraComponent; }

	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

public:

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		UTDSInventoryComponent* InventoryComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components")
		UArrowComponent* DropWeaponArrow;

	TArray<UTDS_MasterEffect*> CurrentEffects;

	TArray<UTDS_MasterEffect*> GetAllCurrentEffects() { return CurrentEffects; }
private:

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

public:
	FName InitWeaponName;
	bool GetFirstInventorySLot();

	ATDSBaseWeapon* CurrentWeapon = nullptr;

	UFUNCTION(BLueprintCallable)
	ATDSBaseWeapon* GetCurrentWeapon() { return CurrentWeapon; }

	void InputAxisX(float Value);
	void InputAxisY(float Value);

	void LookUp(float Value);
	void TurnAround(float Value);

	void StartFire();
	void StopFire();
	void Reload();

	void WeaponInit(FName IdWeapon);
	void PlayAnimation(UAnimMontage* AnimMontage);

	UPROPERTY(EditDefaultsOnly, Category = "HUDWidget")
		TSubclassOf<UTDS_MainHUDWidget> MainHUDWidget;

	UPROPERTY(EditDefaultsOnly, Category = "HUDWidget")
		TSubclassOf<UTDS_StatsWidget> StatsWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category = "PhysMaterial")
		UPhysicalMaterial*  BodyPhysMaterial;

	UPROPERTY(EditDefaultsOnly, Category = "PhysMaterial")
		UPhysicalMaterial*  ShieldPhysMaterial;

	UTDS_MainHUDWidget* HUDWidget;
	UTDS_StatsWidget* StatsWidget;

	void CreateHUDWidget();
	void CreateStatsWidget();

	void SetFirstWeaponClot();
	void SetSecondWeaponSlot();
	void SetThirdWeaponSlot();

	void DropWeapon();
	bool TryGetNewItem(FName ItemName);

	void SetWeaponFromSlot(int32 Slot);
	void UpdateHUDAllAmmo(EWeaponType WeaponType);

	void UpdateAllAmmo();

	void RemoveEffectFromArray(UTDS_MasterEffect* Effect);

	UFUNCTION()
	void OnHealthChange(float Health, float HealthPercent, bool CanSpawnDecal);

	UFUNCTION()
	void OnShieldChange(float Shield, float ShieldPercent);

	UFUNCTION()
	void OnStaminaChange(float StaminaPercent);

	UFUNCTION()
	void SetShieldType();

	UFUNCTION()
	void SetBodyType();

	void TrySpawnDecal();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Decals")
	TArray<FDecalsStruct> BloodDecals;

protected:

	virtual void OnStartRunning() override;
	virtual void OnStopRunnung() override;

	void GoToInventory();
	void ClosingInventory();

	void DestroyCurrentWeapon(bool bIsDrop = false);

	void UpdateHUDWidget();

	void SetPhysMaterial();

	void ChangeRatioForStatsW();
};
