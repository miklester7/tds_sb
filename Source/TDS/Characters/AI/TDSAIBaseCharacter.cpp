// Fill out your copyright notice in the Description page of Project Settings.


#include "Characters/AI/TDSAIBaseCharacter.h"
#include "Characters/AI/TDSAIController.h"

ATDSAIBaseCharacter::ATDSAIBaseCharacter(const FObjectInitializer& ObjInit):Super(ObjInit)
{
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	AIControllerClass = ATDSAIController::StaticClass();
}