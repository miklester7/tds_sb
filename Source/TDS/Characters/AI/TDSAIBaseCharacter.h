// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Characters/MainCharacter.h"
#include "TDSAIBaseCharacter.generated.h"

UCLASS()
class TDS_API ATDSAIBaseCharacter : public AMainCharacter
{
	GENERATED_BODY()
	 
public:
	ATDSAIBaseCharacter(const FObjectInitializer& ObjInit);
};
