// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Types.h"
#include "MainCharacter.generated.h"

class UTDS_CharacterHealthComponent;
class ATDS_GDamageActor;

UCLASS()
class TDS_API AMainCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AMainCharacter(const FObjectInitializer& ObjInit);

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
		UTDS_CharacterHealthComponent* HealthComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<ATDS_GDamageActor> DamageActor;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "SpawnVector", meta = (MakeEditWidget = true))
		FVector DamageWidgetLocation;

	virtual void BeginPlay() override;
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
		ELocomotionType LocomotionType = ELocomotionType::Walk_Type;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed CharacterSpeed;

	UPROPERTY(BlueprintReadWrite)
	bool IsRunning = false;

	UPROPERTY(BlueprintReadWrite)
	bool IsAiming = false;

	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
		void ChangeMovementType(ELocomotionType NewType);

	UFUNCTION()
	void SpawnDamageWidget(const float Damage, FVector& Location, const FLinearColor& Color);

	UFUNCTION()
	void OnDeath();

	virtual void OnStartRunning();
	virtual void OnStopRunnung();

	//UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "TestFunction")
	//	void TestFunction();

	//void TestFunction_Implementation();

	//UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "TestFunction")
		//void TestFunction();

};
