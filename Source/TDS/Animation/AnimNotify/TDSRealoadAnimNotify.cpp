// Fill out your copyright notice in the Description page of Project Settings.


#include "Animation/AnimNotify/TDSRealoadAnimNotify.h"

DEFINE_LOG_CATEGORY_STATIC(NotifyLog, All, All);

void UTDSRealoadAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	UE_LOG(NotifyLog, Display, TEXT("OnNotify"));

	OnNotified.Broadcast(MeshComp);

	Super::Notify(MeshComp, Animation);
}