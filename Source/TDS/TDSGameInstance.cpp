// Fill out your copyright notice in the Description page of Project Settings.


#include "TDSGameInstance.h"

DEFINE_LOG_CATEGORY_STATIC(GameInstanceLog, All, All);

bool UTDSGameInstance::GetWeaponInfoByName(FName WeaponName, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;

	if (!WeaponInfoTable)
	{
		UE_LOG(GameInstanceLog, Error, TEXT("WeaponInfoTable = nullptr"));
		return bIsFind;
	}

	FWeaponInfo* WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(WeaponName, "", false);
	if (WeaponInfoRow)
	{
		bIsFind = true;

		OutInfo = *WeaponInfoRow;
	}

	return bIsFind;
}

bool UTDSGameInstance::GetItemsInfoByName(FName ItemsName, FDropItemsInfo& OutInfo)
{
	if (!PickupItemsTable) return false;

	FDropItemsInfo* ItemsInfoRow = PickupItemsTable->FindRow<FDropItemsInfo>(ItemsName, "", false);
	if (ItemsInfoRow)
	{
		OutInfo = *ItemsInfoRow;
		return true;
	}

	return false;
}

bool UTDSGameInstance::GetAllWeaponRowNames(TArray<FName>& RowNames)
{
	if (!WeaponInfoTable) return false;

	TArray<FName> AllNames = WeaponInfoTable->GetRowNames();

	RowNames = AllNames;

	return true;
}