// Fill out your copyright notice in the Description page of Project Settings.


#include "Settings/TDS_GameSetting.h"

DEFINE_LOG_CATEGORY_STATIC(GameSettingLog, All, All);

void UTDS_GameSetting::SetName(const FText& InName)
{
	Name = InName;
}

void UTDS_GameSetting::SetOption(const TArray<FSettingOption>& InOptions)
{
	Options = InOptions;
}

FSettingOption UTDS_GameSetting::GetCurrentOption() const
{
	const int32 CurrentValue = GetCurrentValue();
	const auto Option = Options.FindByPredicate([&](const auto& Opt) {return CurrentValue == Opt.Value; });

	if (!Option)
	{
		UE_LOG(GameSettingLog, Error, TEXT("Option doesn't exist"));
		return FSettingOption{};
	}

	return *Option;
}

FText UTDS_GameSetting::GetName() const
{
	return Name;
}

void UTDS_GameSetting::AddGetter(TFunction<int32()> Func)
{
	Getter = Func;
}

void UTDS_GameSetting::AddSetter(TFunction<void(int32)> Func)
{
	Setter = Func;
}

void UTDS_GameSetting::ApplyNextOption()
{
	const int32 CurrentIndex = GetCurrentIndex();
	if (CurrentIndex == INDEX_NONE) return;

	const int32 NextIndex = (CurrentIndex + 1) % Options.Num();

	SetCurrentValue(Options[NextIndex].Value);
}

void UTDS_GameSetting::ApplyPrevOption()
{
	const int32 CurrentIndex = GetCurrentIndex();
	if (CurrentIndex == INDEX_NONE) return;

	const int32 PrevIndex = CurrentIndex == 0 ? Options.Num() - 1 : CurrentIndex - 1;

	SetCurrentValue(Options[PrevIndex].Value);
}

int32 UTDS_GameSetting::GetCurrentValue() const
{
	if (!Getter)
	{
		UE_LOG(GameSettingLog, Error, TEXT("Getter func is not set for %s"), *Name.ToString());
		return INDEX_NONE;
	}

	return Getter();
}

int32 UTDS_GameSetting::GetCurrentIndex() const
{
	const int32 CurrentValue = GetCurrentValue();

	return Options.IndexOfByPredicate([&](const auto& Opt) {return CurrentValue == Opt.Value; });
}

void UTDS_GameSetting::SetCurrentValue(int32 Value)
{
	if (!Setter)
	{
		UE_LOG(GameSettingLog, Error, TEXT("Setter func is not set for %s"), *Name.ToString());
		return;
	}

	Setter(Value);
}
