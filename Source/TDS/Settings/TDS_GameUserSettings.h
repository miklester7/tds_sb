// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameUserSettings.h"
#include "TDS_GameUserSettings.generated.h"

class UTDS_GameSetting;

DECLARE_MULTICAST_DELEGATE(FOnSettingsUpdateDelegate);

UCLASS()
class TDS_API UTDS_GameUserSettings : public UGameUserSettings
{
	GENERATED_BODY()

public:
	UTDS_GameUserSettings();

	static UTDS_GameUserSettings* Get();

	const TArray<UTDS_GameSetting*>& GetVideoSettings();

	void RunBenchmark();

	FOnSettingsUpdateDelegate OnVideoSettingsUpdated;

private:
	UPROPERTY()
	TArray<UTDS_GameSetting*> VideoSettings;

};
