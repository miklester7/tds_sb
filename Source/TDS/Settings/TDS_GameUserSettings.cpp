// Fill out your copyright notice in the Description page of Project Settings.


#include "Settings/TDS_GameUserSettings.h"
#include "Settings/TDS_GameSetting.h"

#define BIND_SETTINGS_FUNC(FUNC)\
	[&](int32 Level)\
	{\
		FUNC(Level);\
		ApplySettings(false);\
	}\

#define LOCTEXT_NAMESPACE "GameUserSettings"

UTDS_GameUserSettings::UTDS_GameUserSettings()
{
	const TArray<FSettingOption> VFXOptions =
	{ 
		{LOCTEXT("VFXQualityLow_Loc", "Low"), 0},
		{LOCTEXT("VFXQualityMedium_Loc","Medium"),1},
		{LOCTEXT("VFXQualityHigt_Loc","Higt"),2},
		{LOCTEXT("VFXQualityEpic_Loc","Epic"),3}
	};

	{
		auto* Settings = NewObject<UTDS_GameSetting>();
		check(Settings);
		Settings->SetName(LOCTEXT("Anti - Aliasing_Loc", "Anti - Aliasing"));// ���� ���� - ����������� //�������� ���. key
		Settings->SetOption(VFXOptions);
		Settings->AddGetter([&]() {return GetAntiAliasingQuality(); });
		Settings->AddSetter(BIND_SETTINGS_FUNC(SetAntiAliasingQuality));

		VideoSettings.Add(Settings);
	}


	{
		auto* Settings = NewObject<UTDS_GameSetting>();
		check(Settings);
		Settings->SetName(LOCTEXT("Textures_Loc", "Textures"));
		Settings->SetOption(VFXOptions);
		Settings->AddGetter([&]() {return GetTextureQuality(); });
		Settings->AddSetter(BIND_SETTINGS_FUNC(SetTextureQuality));
		VideoSettings.Add(Settings);
	}


	{
		auto* Settings = NewObject<UTDS_GameSetting>();
		check(Settings);
		Settings->SetName(LOCTEXT("Global Illumination_Loc", "Global Illumination"));
		Settings->SetOption(VFXOptions);
		Settings->AddGetter([&]() {return GetGlobalIlluminationQuality(); });
		Settings->AddSetter(BIND_SETTINGS_FUNC(SetGlobalIlluminationQuality));
		VideoSettings.Add(Settings);
	}


	{
		auto* Settings = NewObject<UTDS_GameSetting>();
		check(Settings);
		Settings->SetName(LOCTEXT("Shadows_Loc", "Shadows"));
		Settings->SetOption(VFXOptions);
		Settings->AddGetter([&]() {return GetShadowQuality(); });
		Settings->AddSetter(BIND_SETTINGS_FUNC(SetShadowQuality));
		VideoSettings.Add(Settings);
	}


	{
		auto* Settings = NewObject<UTDS_GameSetting>();
		check(Settings);
		Settings->SetName(LOCTEXT("Post Processing_Loc", "Post Processing"));
		Settings->SetOption(VFXOptions);
		Settings->AddGetter([&]() {return GetPostProcessingQuality(); });
		Settings->AddSetter(BIND_SETTINGS_FUNC(SetPostProcessingQuality));
		VideoSettings.Add(Settings);
	}

}

UTDS_GameUserSettings* UTDS_GameUserSettings::Get()
{
	return GEngine ? Cast<UTDS_GameUserSettings>(GEngine->GetGameUserSettings()) : nullptr;
}

const TArray<UTDS_GameSetting*>& UTDS_GameUserSettings::GetVideoSettings()
{
	return VideoSettings;
}

void UTDS_GameUserSettings::RunBenchmark()
{
	RunHardwareBenchmark();
	ApplySettings(false);

	OnVideoSettingsUpdated.Broadcast();
}

#undef LOCTEXT_NAMESPACE