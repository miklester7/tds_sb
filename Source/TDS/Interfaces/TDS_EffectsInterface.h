// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TDS_EffectsInterface.generated.h"

UINTERFACE(MinimalAPI)
class UTDS_EffectsInterface : public UInterface
{
	GENERATED_BODY()
};

class TDS_API ITDS_EffectsInterface
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
};
